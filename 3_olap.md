<!--
 see https://github.com/alexeygumirov/pandoc-beamer-how-to
 -->
---
title: "Les systèmes OLAP"
author: "Nicolas Paris"
institute: "Université Parix VIII"
topic: "Entrepôts de Données"
theme: "CambridgeUS"
colortheme: "crane"
fonttheme: "professionalfonts"
outertheme: infolines
fontsize: 11pt
urlcolor: red
linkstyle: bold
aspectratio: 169
date-course: 17/11/2024
section-titles: false
toc-title: Plan du cours
toc: true
---

# OLAP

## Les systèmes OLAP

Les systèmes dits **O**n**L**ine **A**nalytical **P**rocessing (OLAP) sont utilisés pour **analyser** les données et en
tirer de la **connaissance** et des **décisions**.

Ils s'opposent aux systèmes **OLTP** vus dans les systèmes vus dans les chapitres précédents.

## Les 12 règles OLAP
### On-line Analytical Processing (OLAP)
En 1993 Codd définit 12 règles concernant OLAP :

::: columns
:::: column
- (1) Multidimensional Conceptual View
- (2) Transparency
- (3) Accessibility
- (4) Consistent Reporting Performances
- (5) Client/Server Architecture
- (6) Generic Dimensionality
- (7) Dynamic Sparse Matrix Handling

::::
:::: column

- (8) Multi-user Support
- (9) Unrestricted Cross-dimensional Operations
- (10) Intuitive Data Manipulation
- (11) Flexible Reporting
- (12) Unlimited Dimension and Aggregation Levels

::::
:::

## Les systèmes OLTP

Les systèmes dits **O**n**L**ine **T**ransaction **P**rocessing (OLTP). Ils ont pour caractéristiques :

. . .

- accès indexés aux données
- grand nombre d'utilisateurs simultanés
- mises à jour fréquentes
- temps de réponse courts (100 ms à 1 sec)
- gestion de grands volumes de données
- accès à de petits volumes de données

---

### Rappel SGBDR
Les objectifs des SGBDR soumis à la 3 forme normale (3NF) formalisée par Codd :

- transaction
- éviter la redondance
- cohérence des données
- intégrité référentielle
- ACID

---

![Quels ont été les frais de déplacement et le kilométrage des commerciaux de la région Rhône-Alpes ayant des véhicules de
12 à 14 CV en Juillet 1996 ?](img/bi-3nf.png){width=80%}


---

Les limites de la 3NF pour les analyses :

- les modèles sont complexes (complexification pour l'analyste)
- plusieurs chemins entre les entités (ambiguïté l'analyste)
- les indexes sont contreproductifs (structure de données à maintenir)
- les contraintes d'intégrités sont couteuses à vérifier sur l'ensemble des données

---

### Rappel NO-SQL
Les objectifs des bases NO-SQL étaient :

- scalabilité horizontale
- flexibilité sur les modèles de données
- langages de requête spécialisés
- high availability
- BASE

## OLTP VS OLAP

Les systèmes OLTP et OLAP se complètent dans leurs objectifs.

Ils traitent les mêmes données, mais different :

- **OLTP** : Lecture/écritures, très faible proportion des données, très souvent
- **OLAP** : Lecture/écriture, large proportion des données, rarement

### 2 en 1 ?

Lorsque le volume des données à traiter et le nombre d'utilisateurs est faible, les systèmes OLTP peuvent être utilisés
pour réaliser des traitements OLAP avec des avantages et inconvénients :

- `+` limite l'infrastructure à maintenir / maîtriser
- `+` les analyses sont faites sur données en temps réel
- `-` les traitements OLAP risque d'effondrer la base opérationnelle (lock de transaction, IO disques, CPU, temp
  storage ...)
- `-` le modèle de données n'est pas adapté aux deux besoins

# La modélisation dimensionnelle

### Invention
Kimball en 1996 introduit la **modélisation dimensionnelle** et le modèle en flocon.

- dénormalisation (redondance)
- surrogate key
- tables de fait
- tables de dimension

## Faits et Dimensions

### Faits

- le **quoi** à mesurer
- numériques et agrégés
- des clés étrangères non-nulles vers les dimensions
- de l'ordre des millions ou milliards

### Dimensions

- le **par qui, ou, quand** pour partager les données
- attributs textuels, catégoriques, booléens
- souvent hiérarchiques
- de l'ordre des milliers, centaines de milliers

## Le modèle en flocon
![Une table de fait et des dimensions normalisées](img/bi-flocon.png){width=90%}

## Le modèle en étoile

![Une table de fait et des dimensions dénormalisées](img/bi-star.png){width=50%}

## Modèle en constellation

![Des tables de faits partagent des dimensions](img/bi-constellation.png){width=60%}

## Les opérations OLAP

### Drill-Down
::: columns
:::: column

Les données deviennent plus détaillées :

- descendre dans la hiérarchie
- ajouter une dimension

::::
:::: column

![On passe de semestre à mois](img/bi-drill-down.png){width=58%}

::::
:::

---

### Roll-Up
Les données deviennent moins détaillées :

- monter dans la hiérarchie
- réduire les dimensions

![On passe de ville à pays](img/bi-roll-up.png){width=40%}

---

### Dice
On obtient un sous-cube, en fixant plusieurs dimensions.

![On fixe location, time et items](img/bi-slice.png){width=40%}

---

### Slice
On obtient un sous-cube, en fixant une dimension.

![On fixe le temps à Q1](img/bi-slice.png){width=40%}

---

### Pivot
On applique une rotation

![On fixe le temps à Q1, et on pivote](img/bi-pivot.png){width=40%}

## Le Slow Changing Dimension (SCD)

En général, les tables de faits subissent peu de mises à jour : on trace des évènements qui sont ajoutés au fil de
l'eau (append only).

En revanche, les dimensions évoluent : leurs attributs changent avec le temps, de nouvelles modalités sont ajoutées ou
les hierarchies modifiées.

Pour identifier les lignes à modifier, on utilise la clé primaire de la source pour ensuite comparer le contenu des
colonnes. Si changement, alors on applique un SCD1, ou SCD2 (il existe 6 variantes)

---

![Le SCD1 écrase les informations](img/bi-scd1.png){width=100%}

---


![Le SCD2 conserve l'historique](img/bi-scd2.png){width=100%}

## Les bénéfices du model dimensionnel

::: columns
:::: column

### Modèle optimisé

Un seul scan sur chaque table est suffisant. Bénéficie de plans d'exécutions adaptés, du partitionnement vertical,
d'index et de structures spécialisés.

### Clés de jointures manquantes

L'utilisation de **surrogate key** et d'entrées représentant l'absence d'information dans les dimensions, évite les
problèmes et la gestion des NULL.

::::
:::: column

### Simple et classique

Les analystes ont une prise en main rapide du modèle, sans ambiguïté, avec des dimensions systématiques (temporelle,
localisation).

Les ingénieurs ont à disposition des outils simplifiant la transformation et la mise en place.

::::
:::

# Les Cubes OLAP


![Un cube OLAP](img/bi-colap.png){width=45%}

## ROLAP / MOLAP / HOLAP

On distingue plusieurs implémentations pour les cubes :

### ROLAP

**R** pour Relationnel. Le cube est une abstraction basée sur un modèle relationnel

### MOLAP

**M** pour Multidimensionnel. Le cube est géré par une structure dédiée et optimisée (tableaux de bytes
multi-dimensionnels)

### HOLAP

**H** pour Hybride. Le cube est une structure hybride entre le relationnel et des structures dédiées.


## Optimisation des Cubes

Il existe plusieurs approches pour générer les cubes :

1. **Materialisation Physique** du cube entier. L'espace de stockage nécessaire étant corrélé au temps de pré-calcul, ce
   n'est pas adapté aux gros cubes.
2. **Virtualisation** du cube entier. Ne nécessite pas d'espace supplémentaire aux données sources, mais du temps de
   calcul lors des lectures.
3. **Materialisation Partielle** du cube. Un sous-ensemble des cellules du cube sont pré-calculées, et les autres sont
   des fonctions qu'on leur applique.

## Apache Mondrian

### Description

Open-source, Mondrian est un système ROLAP exploitant les SGBDR et les bases SQL OLAP et fournit un **support MDX** pour
interroger le modèle multidimensionnel.

![](img/bi-mondrian.png){width=80%}

## Apache Kylin

### Description

Open-source, Kylin (Ebay 2015) est un système MOLAP exploitant hbase ou spark et fournit un **support SQL** pour
naviguer dans le modèle multidimensionnel.

---

![Architecture Kylin](img/bi-kylin.png){width=80%}


## Le langage MDX

TODO

# Le langage SQL analytique

## Group By

```sql
SELECT select_list
    FROM ...
    [WHERE ...]
    GROUP BY grouping_column_reference [, grouping_column_reference]...
    [HAVING boolean_expression]
```
---

```sql
=> SELECT * FROM test1;
 x | y
---+---
 a | 3
 c | 2
 b | 5
 a | 1
(4 rows)

=> SELECT x, sum(y) FROM test1 GROUP BY x;
 x | sum
---+-----
 a |   4
 b |   5
 c |   2
(3 rows)
```

---

```sql
=> SELECT x, sum(y) FROM test1 GROUP BY x HAVING sum(y) > 3;
 x | sum
---+-----
 a |   4
 b |   5
(2 rows)
```

```sql
SELECT product_id, p.name, (sum(s.units) * (p.price - p.cost)) AS profit
    FROM products p LEFT JOIN sales s USING (product_id)
    WHERE s.date > CURRENT_DATE - INTERVAL '4 weeks'
    GROUP BY product_id, p.name, p.price, p.cost
    HAVING sum(p.price * s.units) > 5000;
```

## Les fonctions d'agrégations

Agrégations classiques :

| Fonction                                    | Description                              |
|:-------------------------------------------:|:----------------------------------------:|
| count(*)                                    | Nombre de lignes                         |
| count ( "any" )                             | Nombre de lignes non-nulles              |
| max("any")                                  | Maximum de valeurs non-nulles            |
| min("any")                                  | Minimum de valeurs non-nulles            |
| sum ( "any" )                               | Sommes des valeurs non-nulles            |
| string_agg ( value, delimiter )             | Concatenation des valeurs, délimitées    |
| array\_agg ( "any" )                         | Collection des valeurs dans un tableau   |
| json\_object\_agg ( key, value )              | Collection de clé/valeurs dans un JSON   |

---

Agrégations statistiques :

| Fonction                                    | Description                              |
|:-------------------------------------------:|:----------------------------------------:|
| avg ( "any" )                               | Moyenne des valeurs non-nulles           |
| corr ( Y double , X double  )               | Coefficient de corrélation des valeurs   |
| regr\_r2 ( Y double , X double  )           | R2 sur les valeurs                       |
| variance ( numeric\_type )                  | Variance sur les valeurs                 |
| percentile\_disc ( fraction )               | Médiane quand fraction                   |
|  WITHIN GROUP ( ORDER BY col )              | est égale à 0.5                          |

## Group By Sets
```sql
=> SELECT * FROM items_sold;
 brand | size | sales
-------+------+-------
 Foo   | L    |  10
 Foo   | M    |  20
 Bar   | M    |  15
 Bar   | L    |  5
(4 rows)

=> SELECT brand, size, sum(sales) FROM items_sold GROUP BY GROUPING SETS ((brand), (size), ());
 brand | size | sum
-------+------+-----
 Foo   |      |  30
 Bar   |      |  20
       | L    |  15
       | M    |  35
       |      |  50
(5 rows)
```
## Rollup
**Rollup** est un sucre syntaxique de **grouping sets**. Il est adapté aux dimensions hiérarchiques :

```sql
GROUP BY ROLLUP ( a, b, c )
```
Équivaut à :

```sql
GROUP BY GROUPING SETS (
    ( a, b, c ),
    ( a, b ),
    ( a ),
    ( )
)
```

## Cube
**Rollup** est un sucre syntaxique de **grouping sets**. Il est adapté aux dimensions hiérarchiques :

```sql
GROUP BY CUBE ( a, b, c )
```
Équivaut à :

```sql
GROUP BY GROUPING SETS (
    ( a, b, c ),
    ( a, b    ),
    ( a,    c ),
    ( a       ),
    (    b, c ),
    (    b    ),
    (       c ),
    (         )
)
```

## Windows Functions

Syntaxe:

```sql
function_name ([expression [, expression ... ]])
[ FILTER ( WHERE filter_clause ) ]
OVER (
[ PARTITION BY expression [, ...] ]
[ ORDER BY expression [ ASC | DESC | USING operator ] [ NULLS { FIRST | LAST } ] [, ...] ]
)
```

### Principe

Permet des calculs sur des groupes de lignes. Chaque ligne reste séparée en sortie, contrairement aux **group by**.

---

| Fonction                        | Description                              |
|:-------------------------------:|:----------------------------------------:|
| row_number()                    | Nombre dans la partition courante        |
| rank()                          | Rang dans la partition courante, avec écart |
| dense_rank()                    | Rang dans la partition courante, sans écart |
| lag ( value [, offset ] )       | N-ième valeur avant la ligne dans
|                                 |      la partition courante |
| lead ( value [, offset ] )      | N-ième valeur après la ligne dans
|                                 |      la partition courante |
| first_value ( value  )          | Première valeur dans la partition courante |
| last_value ( value  )           | Dernière valeur dans la partition courante |
| nth_value ( value, n )          | N-ième valeur dans la partition courante   |


---

Différences entre row_number, rank et dense_rank:

```sql
WITH t(v) AS (VALUES ('a'),('a'),('a'),('b'),('c'),('c'),('d'),('e') )
SELECT v,
 ROW_NUMBER() OVER(ORDER BY v),
 RANK()       OVER(ORDER BY v),
 DENSE_RANK() OVER(ORDER BY v)
FROM t
ORDER BY 1, 2;
+---+------------+------+------------+
| v | row_number | rank | dense_rank |
+---+------------+------+------------+
| a |          1 |    1 |          1 |
| a |          2 |    1 |          1 |
| a |          3 |    1 |          1 |
| b |          4 |    4 |          2 |
| c |          5 |    5 |          3 |
| c |          6 |    5 |          3 |
| d |          7 |    7 |          4 |
| e |          8 |    8 |          5 |
+---+------------+------+------------+
```


## Le problème des count distinct

Considérons la table suivante avec une granularité à la seconde. Réaliser des analyses est couteux en calculs du fait de
du nombre important de lignes.

```sql
select event_id, event_type, user_id, created_at
from github_events limit 10;
  event_id  | event_type  | user_id  |     created_at
------------+-------------+----------+---------------------
 4950825375 | PushEvent   |  1159138 | 2016-12-01 05:22:40
 4950825381 | PushEvent   | 10810283 | 2016-12-01 05:22:41
 4950825388 | WatchEvent  |  1171503 | 2016-12-01 05:22:41
 4950825393 | PushEvent   |  2862206 | 2016-12-01 05:22:41
 4950825395 | PushEvent   | 12483820 | 2016-12-01 05:22:41
 4950831394 | CreateEvent |   923865 | 2016-12-01 05:25:00
 4950825417 | PushEvent   | 13530156 | 2016-12-01 05:22:42
 4950825423 | PushEvent   |  7155653 | 2016-12-01 05:22:42
```

## Pré-calcul des Rollup

On peut pré-agréger des tables pour simplifier les calculs :

```sql
SELECT
    date_trunc('minute', created_at) AS created_at,
    COUNT(distinct user_id) AS event_count
FROM github_events
GROUP BY 1 limit 10;
     created_at      | event_count
---------------------+-------------
 2016-12-01 05:20:00 |           1
 2016-12-01 05:21:00 |           2
 2016-12-01 05:22:00 |         141
 2016-12-01 05:23:00 |         441
 2016-12-01 05:24:00 |         416
```

Cependant, on ne peut pas en déduire le nombre distinct par heure. Ni filtrer par event_type. Il faudra créer d'autres
tables de rollup adaptées à chaque question.


## HyperLogLog (HLL)

### Principe

Les requêtes **count distinct** ont une complexité O(n). Les algorithmes HLL ont de meilleures performances au prix de
résultats approximatifs. Il **hash** les valeurs et utilise leurs propriétés statistiques pour déduire le nombre
d'occurrences. Plusieurs fonctions de hash sont utilisées et leur moyenne offre des résultats plus précis.

---

On prépare une table pour les HLL avec les hash :

```sql
INSERT INTO github_events_rollup_minute(
    created_at,
    event_type,
    distinct_user_id_count
)
SELECT
    date_trunc('minute', created_at) AS created_at,
    event_type,
    hll_add_agg(hll_hash_bigint(user_id))
FROM github_events
GROUP BY 1, 2;

     created_at      |          event_type           |          distinct_user_id_count
---------------------+-------------------------------+------------------------------------------
 2016-12-01 05:04:00 | PullRequestReviewCommentEvent | \x128b7fa3f487b8abb6697b
 2016-12-01 05:16:00 | PullRequestReviewCommentEvent | \x128b7fd312b413a8fd1681
 2016-12-01 05:20:00 | PullRequestReviewCommentEvent | \x128b7fc3211ea175230a03
```

---

Agrégation sur event_type classique :

```sql
SELECT
    event_type,
    count(distinct user_id) AS distinct_count
FROM
    github_events
GROUP BY event_type
ORDER BY event_type;
          event_type           | distinct_count
-------------------------------+----------------
 CommitCommentEvent            |            164
 CreateEvent                   |           8442
 DeleteEvent                   |           1216
 ForkEvent                     |           4000
 GollumEvent                   |            251
 IssueCommentEvent             |           4779
 IssuesEvent                   |           2553
 MemberEvent                   |            410
Time: 208,786 ms
```

---

Agrégation sur event_type via HLL :

```sql
SELECT
    event_type,
    hll_cardinality(hll_union_agg(distinct_user_id_count)) AS distinct_count
FROM
    github_events_rollup_minute
GROUP BY event_type ORDER BY event_type;
          event_type           |   distinct_count
-------------------------------+--------------------
 CommitCommentEvent            | 161.17930997775483
 CreateEvent                   |  8467.075634025996
 DeleteEvent                   |  1196.201156492171
 ForkEvent                     | 3961.3635211987726
 GollumEvent                   | 249.61182802851314
 IssueCommentEvent             |  4805.640861550349
 IssuesEvent                   |   2555.68289435806
 MemberEvent                   |  408.0867310228416
Time: 17,171 ms
```

---

::: columns
:::: column
```sql
SELECT
    EXTRACT(HOUR FROM created_at) AS hour,
    count(distinct user_id) AS distinct_push_count
FROM
    github_events
WHERE
    created_at BETWEEN '2016-12-01 00:00:00'::timestamp
    AND '2016-12-01 23:59:59'::timestamp
    AND event_type = 'PushEvent'::text
GROUP BY 1
ORDER BY 1;
 hour | distinct_push_count
------+---------------------
    5 |                6059
    6 |                9482
    7 |               10370
    8 |                6936
Time: 104,573 ms
```

::::
:::: column

```sql
SELECT
    EXTRACT(HOUR FROM created_at) AS hour,
    hll_cardinality(hll_union_agg(distinct_user_id_count)) AS distinct_push_count
FROM
    github_events_rollup_minute
WHERE
    created_at BETWEEN '2016-12-01 00:00:00'::timestamp
    AND '2016-12-01 23:59:59'::timestamp
    AND event_type = 'PushEvent'::text
GROUP BY 1
ORDER BY 1;
 hour | distinct_push_count
------+---------------------
    5 |   6206.615864985464
    6 |    9517.80542100396
    7 |  10370.408764016578
    8 |   7067.260738103565
Time: 6,353 ms
```

::::
:::

---

Conclusion sur les HLL :

- bonnes performances sur les count distincts
- manipulations possibles sous différents axes
- résultats non exacts

## Les CTE

Le mot clé `with` permet de nommer des **C**ommon **T**able **E**xpression (CTE) :

- améliore la lisibilité de requêtes complexes par modularisation
- mise en cache de CTE si ré-utilisées à plusieurs reprises
- une CTE peut être un select, insert, delete ou update

```sql
WITH covid_patient AS ( SELECT * FROM patient WHERE covid IS TRUE ),
group_covid AS ( SELECT count(*), gender FROM covid_patient GROUP BY gender ),
insert_tmp AS ( INSERT INTO tmp_patient SELECT * FROM group_covid ),
total_children AS ( SELECT count(*) FROM covid_patient WHERE age <= 18 )
SELECT total FROM total_children
```

## Merge

```sql
MERGE INTO <target table> AS T USING <source expression/table> AS S
ON <boolean expression1>
WHEN MATCHED [AND <boolean expression2>] THEN UPDATE SET <set clause list>
WHEN MATCHED [AND <boolean expression3>] THEN DELETE
WHEN NOT MATCHED [AND <boolean expression4>] THEN INSERT VALUES<value list>
```

### Le Merge

Cette opération SQL permet de réaliser efficacement des SCD1 et SCD2 avec des optimisations de calculs pour des gros
volumes de données avec une syntaxe claire.

# Le Massively Parallel Processing

## Le stockage colonne

::: columns
:::: column

![Colonnes contigües dans un fichier](img/bi-row-store.png){width=76%}

::::
:::: column
![Chaque colonne dans un fichier](img/bi-column-store.png){width=100%}

::::
:::

---

![Accès aux colonnes d'une ligne: 1 random read VS 3 pour colonnaire](img/bi-store-row-access.png){width=80%}

---

![Accès à une colonne pour toutes lignes: 3 fois moins d'I/O pour colonnaire](img/bi-store-row-access.png){width=80%}

## Greenplum
### Principe
Greenplum est open-source et maintenu par VM-ware. C'est un fork de postgreSQL avec des fonctionnalités OLAP :

- supporte le SQL analytic
- clé de distribution à définir pour chaque table
- stockage colonne ou ligne
- compression gzip des colonnes
- planificateur de requête adapté aux systèmes distribués
- gestion des utilisateurs par files
- gestion du json et du full-text

## Citus
Citus est open-source et maintenu par Microsoft. C'est une extension de postgreSQL avec des fonctionnalités OLAP :

- clé de distribution à définir pour chaque table
- supporte le SQL analytic
- stockage colonne ou ligne
- co-location des tables
- gestion du json et du full-text

---

Considérons les tables suivantes pour illustrer la **co-location** :

```sql
CREATE TABLE event (
  tenant_id int,
  event_id bigint,
  page_id int,
  payload jsonb,
  primary key (tenant_id, event_id)
);

CREATE TABLE page (
  tenant_id int,
  page_id int,
  path text,
  primary key (tenant_id, page_id)
);
```

---


![Distribution sur event\_id / page\_id : Requêtes croisées inefficientes](img/bi-citus-co1.png){width=80%}

---

![Distribution sur tenant\_id : Requêtes optimale](img/bi-citus-co2.png){width=80%}


## Redshift
Système propriétaire édité par Amazon basé sur postgreSQL.

- clé de distribution à définir pour chaque table
- supporte le SQL analytic
- stockage colonne
- compression
- gestion des utilisateurs par files

## Clickhouse

Clickhouse est open-source et maintenu par Yandex. Au prix de certains sacrifices, c'est un moteur OLAP très efficient :

- SQL étendu : gestion avancée de structures imbriquées / tableau
- pas de gestion de transactions
- limitation sur les update / delete

## Conclusion sur les bases MPP

Les systèmes Massively Parallel Processing (MPP) sont adaptés aux volumes moyens (tera-octets) :

Avantages :

- efficients et adaptés au reporting (tableau de bord)
- supportent les mises à jour / suppression
- supportent les transactions
- supportent le SQL analytique

Inconvénients :

- non prévus pour des milliers d'instances
- pas ou peu résilients aux pannes (une instance en panne met à mal l'ensemble)
- gèrent mal les données non-structurées (texte, images, video ...)
- coûts infrastructure important (SSD / RAM)
- couplage entre stockage et calculs (quand pas de calculs, machines sous-utilisées pour le stockage)

---

![Commits greenplum annuel](img/bi-greenplum-commit.png){width=64%}

![Commits citus annuel](img/bi-citus-commit.png){width=64%}

![Commits clickhouse annuel](img/bi-clickhouse-commit.png){width=64%}

# Hadoop

## Architecture

Hadoop est une plateforme constituée de différentes briques requises (HDFS, Yarn, Zookeeper) et optionnelles (
Spark, Hbase, Pig ...) pour la majorité écrites en java et sous license Apache.

![Le framework hadoop](img/bi-hadoop.png){width=64%}


## HDFS

![La réplication x3 des données HDFS](img/bi-hdfs.png){width=75%}

## Le Map Reduce

![Le Map Reduce](img/bi-mapreduce-shuffle.png){width=80%}

---

Le paradigme d'implémentation Map-Reduce, est hautement distribuable et tolérant aux pannes : un worker peut en
remplacer un autre, recommencer l'opération de map ou de reduce.

### Le Map

Un certain nombre de workers sont des **Mapper**. Ils appliquent une fonction `Map(k1,v1) -> list(k2,v2)`

### Le Shuffle

Il consiste à envoyer les paires issues des **Mappers** vers les **Reducers**, via le réseau en fonction de la clé en
sortie du mapper (`k2`)

### Le Reduce

Un certain nombre de workers sont des **Reducer**. Ils appliquent une fonction `Reduce(k2, list (v2)) -> list((k3, v3))`

## Conclusion sur Hadoop

Avantages :

- machines bas de gamme nécessaires
- nombreuses briques spécialisées et efficaces
- résilients aux pannes
- dizaines de milliers de nœuds

Inconvénients :

- couplage entre stockage et calculs (quand pas de calculs, machines sous-utilisées pour le stockage)
- le map-reduce est dépassé
- problématiques de fournisseurs hadoop

---

Hadoop a décliné. Ses points forts (hdfs, map-reduce, hbase) sont concurrencées par des approches cloud plus abordables.

Les éditeurs historiques d'Hadoop (Hortonworks et Cloudera) ont fusionné, voire disparu (MapR), pour laisser une période
d'inactivité et de doute.

Il reste largement implanté dans de très nombreuses sociétés et une distribution Hadoop open-source est active : **
Apache Bigtop**.

# Les systèmes NewSql

## Les formats Row-Column
### Principe

Le format colonne est efficient, cependant il rend difficile la réconciliation des colonnes. C'est-à-dire la lecture
ligne à ligne à partir de différents fichiers de colonnes. Avec le format row-column, les données d'une même ligne sont
dans le même fichier.

![Le format Row-Column](img/bi-parquet-rc.png){width=70%}

---

![Le format Parquet](img/bi-parquet.png){width=60%}

---

Le format parquet a pour caractéristiques :

- format hybride **row columnar**
- binaire
- compression (gzip, snappy ...)
- compression par dictionnaire
- nested (données structure imbriquée)
- statistiques (footer)
- métadonnées de structures (footer)
- immutable !

---

Les statistiques contenues dans le footer permettent le **predicate push-down** :

![Les statistiques permettent d'éviter de lire le fichier parquet (rouge)](img/bi-parquet-pushdown.png){width=35%}

---

### Le partitioning

On utilise une arborescence de dossier pour séparer les fichiers parquets en fonction d'une/des colonnes. Lorsqu'un
prédicat concerne cette colonne, cela permet d'éviter la lecture des dossiers / sous-dossiers incompatibles.

Inconvénient :

- le nombre de modalités des colonnes doit rester faible, pour éviter une trop grande arborescence
- inutile pour les requêtes ne comportant pas le prédicat sur la colonne de partition
- augmente le nombre de fichiers

---

![Fichiers parquets partitionnés par genre et filière](img/olap-parquet-part.png){width=40%}

---

### Le clustering

On pré-ordonne les lignes avant d'écrire les fichiers parquets, sur une/des colonnes à choisir. Lorsqu'un prédicat
concerne cette colonne cela permet d'optimiser le push-down (éviter de lire des fichiers grâce aux statistiques)

Inconvénient :

- temps de calcul supplémentaire lors de l'écriture
- inutile pour les requêtes ne comportant pas le prédicat sur la colonne de clustering

---

### Le bucketing

On dispatche les lignes dans des fichiers en fonction du résultat du hash d'une/des colonnes. Lorsque plusieurs tables
ont un même bucketing, les jointures sur la colonne choisie pourront être traités par lots de bucket ayant la même clé
de hash sans avoir à vérifier que le contenu de chaque fichier est identique.

Inconvénient :

- temps de calcul supplémentaire lors de l'écriture
- inutile pour les requêtes n'ayant pas de jointures sur la colonne de bucket
- augmente le nombre de fichiers

## Hive

### Principe

Hive est une brique OLAP pour Hadoop qui permet de gérer les fichiers parquet au sein de tables. Les informations de
structures des tables sont stockées dans le Hive Metastore, qui est une base de données relationnelle.

Hive fonctionne avec plusieurs moteurs :

- le map-reduce (présenté avant)
- TEZ (une optimisation de Map-Reduce)
- SPARK (présenté après)
- LLAP (Un moteur in-memory adapté aux requêtes interactives exploratoires)

---

Hive exploite les fichiers parquets présents sur HDFS via des requêtes SQL. Il hérite des propriétés de scalabilité et
de résilience de Hadoop :

![Hive sur Hadoop](img/bi-hive-arch.png){width=60%}

---

Hive supporte un dialecte SQL : le HQL (reduction/extension du SQL).

Par exemple Hive est l'un des seuls moteurs SQL à supporter le multi-table insert (insérer dans plusieurs tables en ne
lisant qu'une fois les données sources) :

```sql
FROM from_statement
INSERT INTO TABLE tablename1 [PARTITION (partcol1=val1, partcol2=val2 ...)] select_statement1
[INSERT INTO TABLE tablename2 [PARTITION ...] select_statement2 ]
```


---

Les SQL est transformé en Map-Reduce. Exemple pour l'agrégation Hive :

```sql
Select Col1, Sum(Col2), Avg(Col3) from Table1 where Col4 = "X" group by Col1
```

::: columns
:::: column

On filtre sur col4 et on récupère Col2 et Col3 :

```scala
Map(k, record) {
  If (record.Col4 == "X") {
  outRecord = (record.Col2, record.Col3)
  collect(Col1, outRecord)
  }
}
```

::::
:::: column

On Reduit par modalité de Col1, et on retourne Sum et Avg :

```scala
Reduce (k, listOfRecords) {
    Sum = Avg = 0
    foreach record in listOfRecords {
        Sum += record.Col2
        Avg += record.Col3
    }
    Avg = Avg / length(listOfRecords)
    outputRecord = (Sum, Avg)
    emit(k, outputRecord)
}
```
::::
:::

---

Hive maintient des statistiques sur les tables dans le metastore (nombre de lignes, taux de null, unicité...).

Le **C**ost **B**ased **O**ptimizer (CBO) permet d'optimiser les opérations de calculs :

- broadcaster les petites tables
- réordonner les joins
- paralléliser les joins

---

Le broadcast-join:

Des opérations SQL comme **join** ou **group by** nécessitent un **shuffle** des données entre les nœuds. Comme pour le
map-reduce, le shuffle consiste à répartir les mêmes clés sur les mêmes nœuds pour réaliser la jointure.

Hive offre des optimisations pour le modèle dimensionnel pour éviter ces échanges couteux : le **map join** (ou
broadcast join).

### Principe

Le **broadcast-join** consiste à envoyer (broadcaster) les petites tables de jointures sur tous les nœuds pour éviter de
déplacer la plus grosse table. La jointure se réalise en déplaçant le minimum de données. (shuffle minimal)

---

![Les broadcast-join](img/bi-hive-broadcast.png){width=80%}

---

Le volume de données temporaires écrites est plus faible si l'ordre des jointures est optimal.

![Réordonner les jointures](img/bi-cbo_1.png){width=45%}

---

Les jointures des modèles en flocons peuvent être parallélisées.

![Le cas des modèles en flocon](img/bi-cbo_2.png){width=50%}

---

Hive gère les transactions au niveau d'une table via le MVCC (voir cours 1)

- lock exclusif lors d'une écriture

![Merge SQL](img/bi-hive-merge.png){width=55%}


## Spark
### Principe
Spark fonctionne sur Hadoop, mais aussi sur le cloud (Kubernetes et S3).

Spark couvre :

- spark-sql: son dialecte SQL. Comme HIVE, il inclue les fonctions SQL analytiques.
- graphX: un moteur de graph supportant les langages pregel et cypher
- MLlib: une suite d'outil de machine learning
- spark-streaming : un outil de gestion de données temps-réel

---

Un job spark comprend un driver et une grappe de workers, qui réalisent des traitements et peuvent communiquer des
données.

![Architecture spark](img/bi-spark-arch.png){width=65%}

---

Un **R**esilient **D**istributed **D**ataset (RDD), est un tableau de données partitionné dont chaque partition subit
des opérations en mémoire RAM.

![L'unité de travail de spark: le RDD](img/bi-spark-rdd.png){width=55%}

--

Spark a son moteur d'optimisation, qui raisonne sur l'arbre des transformations (DAG) dérivé de la requête SQL :

![L'optimiseur "Catalyst"](img/bi-spark-catalyst.png){width=65%}

---

Comme hive, spark gère les transactions au niveau d'une table, grâce au MVCC et à l'approche "Copy on Write" :

![Apache Hudi](img/bi-spark-hudi.png){width=70%}


---

Deux approches pour modifier une ligne dans un fichier parquet **immutable** :

::: columns
:::: column
### Copy On Write

On crée un nouveau fichier parquet identique, en changeant la ligne, et on supprime le précédent. Cette approche est **
lente en écriture**, **rapide en lecture**.

::::
:::: column

### Merge On Read

On ajoute un fichier avec la nouvelle ligne et on indique que ce fichier remplace la ligne. Le reader, réconcilie
l'information à la lecture. Cette approche est **rapide en écriture**, **lente en lecture**.

::::
:::

## Snowflake

### Principe

Snowflake est un système OLAP dédié au **cloud**. Le stockage est découplé de la puissance de calcul. L'infrastructure
matérielle peut être optimisée selon les besoins.


![Snowflake](img/bi-snowflake.png){width=40%}

## BigQuery

Propriétaire et édité/utilisé par google, l'architecture est publiée sous le nom de Dremel en 2010.

![BigQuery et Dremel](img/bi-dremel.png){width=45%}

## Trino

### Principe

Open-source, c'est un moteur SQL multi-sources (HIVE, postgreSQL, mongoDB, cassandra, clickhouse ...) soit la plupart
des systèmes étudiés ici. Plusieurs forks existent dont Presto (facebook) et Athena (amazon).


![Trino architecture](img/bi-trino.png){width=45%}

## Conclusion sur le NewSql


Le NewSql est à privilégier pour la gestion des larges volumes (peta-octet) et concurrence hadoop :

Avantages :

- résilience supérieure aux MPP
- scalabilité supérieure aux MPP
- SQL avancé
- coûts faibles

Inconvénients :

- transactions très limitées / coûteuses
- implémentations UPDATE / DELETE limitées (immutabilité)
- moins réactifs que les MPP (impacts tableau de bord interactif)

# Références

- Data Warehouse et Décisionnel - Pierre Morizet-Mahoudeaux
- The Data Warehouse Toolkit - Ralph Kimball
- Mondrian in Action - Julian Hyde
- Database System Architecture - Matei Zaharia
- SQL on Big Data - Sumit Pal
- Wikipedia
