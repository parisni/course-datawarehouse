<!--
 see https://github.com/alexeygumirov/pandoc-beamer-how-to
 -->

---
title: "Data Warehouse"
author: "Nicolas Paris"
institute: "Université Paris VIII"
topic: "Entrepôts de Données"
theme: "CambridgeUS"
colortheme: "crane"
fonttheme: "professionalfonts"
outertheme: infolines
fontsize: 11pt
urlcolor: red
linkstyle: bold
aspectratio: 169
date-course: 2024-12-01
section-titles: false
toc-title: Plan du cours
toc: true
---

# Le data warehouse


### Un Entrepôt de Données (data warehouse)

Un ensemble de données et métadonnées orientées-sujets, intégrées, temporelles, non-volatiles destinées à l'aide à la
décision via le reporting, le data-mining, les requêtes analytiques et le machine-learning.

---

![L'histoire autour des data warehouse](img/dwh-history.png){width=90%}

---

Les parents du Data Warehouse :

::: columns
:::: column

![Bill Inmon](img/dwh-inmon.png){width=60%}

::::
:::: column

![Ralph Kimball](img/dwh-kimball.png){width=58%}

::::
:::


---

![Les relations entre BI, Data Warehouse et Analytics](img/dwh-enjeux.png){width=90%}

---

Les caractéristiques du Data Warehouse selon Bill Inmon (1992) :

::: columns
:::: column

### Orientées-sujets
Organisation par sujets (ventes, produits, clients, patients) en focalisant sur l'information pertinente. La
modélisation simplifie le sujet, qui était orienté transaction dans les systèmes opérationnels.

### Intégrées
Les données sont alignées dans l'entrepôt, pour offrir une vue homogène et intègre.

::::
:::: column

### Non-volatiles
Les données sont historisées pour que les modifications dans le temps soient mises en évidence.

### Temporel
Chaque information est valable à un certain moment dans le temps.

::::
:::

---

![Temporel et non-volatile](img/dwh-caract.png){width=65%}

# L'architecture du Data Warehouse

![Architecture Générale](img/dwh-arch-general.png){width=90%}

---

### Data Marts (magasins de données)

Tandis qu'un Data Warehouse couvre l'ensemble, les Data Marts est spécifique d'un sujet ou département. Il peut dériver
du Data Warehouse, ou directement des données opérationnelles (indépendant). Il suit la modélisation dimensionnelle.

### Data Store Opérationnel (ODS)

Vue récente sur les données dédiées aux outils opérationnels. À ne pas confondre avec les bases opérationnelles : l'ODS
offre une vue consolidée. Bill Inmon a décrit plusieurs variantes d'ODS en 1996, qui alimentent ou dérivent du data
warehouse.

---

![Un ODS dérivé du data warehouse](img/dwh-arch-ods-derived.png){width=60%}


## Les différentes architectures

### Data Marts indépendants

Chaque silo de l'entreprise produit un data-mart. Ils n'ont pas de relations et ne partagent aucune données communes. Il
n'y a pas de travail d'alignement et d'équivalence.

![Magasin de données indépendants](img/dwh-arch-indep-dm.png){width=90%}

---

### L'architecture Bus

Les data-mart sont modélisés de manière dimensionnelle et les constellations résultantes sont donc liées et une vision
complète de l'entreprise est rendue possible. C'est la méthode de Ralph Kimball.

![Bus de données](img/dwh-arch-bus.png){width=90%}

---

### L'architecture Hub

L'ensemble des silos est centralisé de manière normalisée. De plus, l'entrepôt met à disposition des magasins de
données. C'est la méthode de Bill Inmon.

![Hub de données](img/dwh-arch-hub.png){width=90%}

---

### Architecture Centralisée

L'ensemble des silos est centralisée de manière normalisée. L'entrepôt ne met pas à disposition de magasins de données.
Au besoin, les utilisateurs finaux réalisent leurs propres traitements.

![Entrepôt centralisé](img/dhw-arch-central.png){width=90%}

---

### Architecture Fédérée

Chaque silo maintient un système d'entrepôt spécifique. L'entrepôt de fédéré les interroge à la volée, sans dupliquer
l'information, via des requêtes logiques pour unifier les données.


![Entrepôt Fédéré](img/dhw-arch-federated.png){width=90%}

## L'avantage du Hub

![Un data mart par cas d'usage, dérivé de modèles normalisés](img/dwh-hub.png){width=47%}

## La difficulté de la centralisation

![Chaque entrepôt a son propre modèle](img/dwh-central-pb1.png){width=45%}

---

![Standardisation des modèles complexe nécessaire avant mise en commun](img/dwh-central-pb2.png){width=62%}

# L'Extraction
## Les bases opérationnelles

Récupération dénormalisée des nouvelles données entre le 28 et le 29/11/2021 :

```sql
SELECT
    posts.post_id,
    post.content, posts.date_maj, posts.date_ins, posts.date_del,
    users.user_id,
    users.name, users.date_maj AS users_date_maj, users.date_ins AS users_date_ins, users.date_del AS users_date_del
FROM
    users,
    posts USING (user_id)
WHERE
    users.date_maj BETWEEN '2021-11-28' AND '2021-11-29'
    OR users.date_ins BETWEEN '2021-11-28' AND '2021-11-29'
    OR users.date_del BETWEEN '2021-11-28' AND '2021-11-29'
    OR posts.date_maj BETWEEN '2021-11-28' AND '2021-11-29'
    OR posts.date_ins BETWEEN '2021-11-28' AND '2021-11-29'
    OR posts.date_del BETWEEN '2021-11-28' AND '2021-11-29'
```

---

- paramétrable avec des dates de début/fins (tous les jours, semaines...)
- impossible à mettre en œuvre si dates absentes (ins/maj/del)
- dans l'idéal, éviter les jointures pour limiter l'impact sur le SGBDR et les réaliser par la suite
- des dizaines de requêtes différentes sont nécessaires pour extraire les données
- le modèle de données peut changer (type de colonnes, contenu migré dans d'autres tables...)

## Les documents non structurés

Apache Tika permet d'extraire le contenu de fichiers (pdf, docx, excel...) vers des formats exploitables (text, json,
xml...). Ces formats peuvent être alors re-structurés pour être exploités ou chargés dans des moteurs de recherches (cf
cours 1 et 2).

```bash
usage: java -jar tika-app.jar [option...] [file|port...]
   -x  or --xml           Output XHTML content (default)
   -t  or --text          Output plain text content
   -j  or --json          Output metadata in JSON
   -d  or --detect        Detect document type
```

## EAI
### Enterprise Application Integration

Un EAI permet à l'entreprise d'interconnecter des outils qui ne sont pas prévus pour fonctionner ensemble. Il achemine
des messages d'un point à un autre en flux ou en lots.

L'EAI peut être une source d'information pour un ETL. Il a l'avantage d'être en temps réel, géré/maîtrisé en interne
avec des formats interopérables.

Exemple d'outils :

- apache Camel
- spring integration
- Open ESB (outil graphique)


# Les Transformations

De nombreuses transformations sont courantes :

- dénormalisation (jointure)
- fusion / explosion de colonnes
- enrichissement de données
- ajout de clés surrogates
- NLP (traitement des textes)
- fonctions d'agrégations

---

### ETL

Les **t**ransformations sont traitées après l'**e**xtraction et avant le chargement (**l**oad). C'est resté la tendance
jusqu'aux années 2010.

- les calculs sont réalisés sur une machine dédiée
- pression faible sur la base opérationnelle et le data-warehouse
- des languages et frameworks adaptés peuvent être utilisés (java, python, spark ...)
- des données doivent aussi être extraites du data-warehouse pour récupérer les clés surrogate (dimensions)

---

### ELT

Les **t**ransformations sont traitées après le chargement (**l**oad). C'est la nouvelle tendance.

- les languages utilisés sont plus limités (fonctions SQL, procédures stockées)
- les calculs sont faits par la base stockant le data-warehouse (optimisation des coûts serveurs / concurrence sur les ressources)
- locks de tables pendant les transactions
- limitation des transferts réseaux de données (in-place)


# Le chargement

## Chargement par lot

### Load

L'étape de chargement permet d'intégrer définitivement les données dans le data-warehouse, en batch (lot).

- SCD1, SCD2 (voir cours 3)
- transactionnel (si possible)

---

Exemple de SCD1 transactionnel :

```sql
BEGIN;
insert into posts (...) select ... from load_posts join posts using (posts_id) where load_posts.post_source_id is null;
update posts set (...) from load_posts where load_posts.post_id = posts.post_id ;
COMMIT;
```

## Annule et remplace
### Overwrite
On peut supprimer les informations du data-warehouse et reprendre la source, en partie ou complètement :

1. Full : On détruit toutes les données, et on ré-écrit.
2. Partitionnement : On supprime une partition, et on ré-écrit les données. Par exemple, on partitionne les données par
   mois, et chaque jour, on efface la partition du mois courant, qu'on ré-écrit aussitot.

Dans l'idéal, on réalise cette opération dans une transaction, pour ne pas affecter les utilisateurs.

## La reprise d'historique
### Reprise d'historique
Lors de l'intégration d'une source, on doit récupérer l'antériorité. Il y a deux options :

1. Initialisation: On récupère tout l'historique en un lot. Cela nécessite un développement spécifique (davantage de
   ressources de calculs et retirer les filtres sur la source)
2. Backfill: On rejoue le temps en simulant un retour en arrière. On utilise le même ETL, en configurant les dates
   dans le passé.

L'initialisation est plus rapide que le backfill, mais moins robuste.


# Les outils ETL/ELT
## Les outils graphiques

![Pentaho Keetle](img/dwh-keetle.png){width=93%}

---

![Talend Integration](img/dwh-talend.png){width=90%}

---

### Développement graphique

Talend et Keetle, permettent de développer des ETL ou des ELT de manière visuelle. Un job est transformé en programme
java qui peut être déployé et exécuté sur un serveur de calculs. Ce dernier doit avoir accès :

- aux servers sources de données
- au data-warehouse

L'accès nécessite l'ouverture de firewall et la création de roles pour l'authentification aux différentes bases de
données.

---

Les outils de développement d'intégration de données graphiques :

- nécessitent peu de programmation
- les rendus visuels permettent des discussions multi-compétences
- favorisent le copier/coller
- limitent l'automatisation
- limitent les tests unitaires / intégration
- s'intègre très mal aux VCS (systèmes de version de code). Exemple : git
- permettent de démarrer rapidement un projet
- rebute les programmeurs
- néfaste sur un projet à long terme

## Les approches programmatiques

::: columns
:::: column

### Java

Java est le langage de la data-ingénierie. Il est adapté aux traitements de données et au multi-processing.

- spark
- spring

### Python

Python est plus simple, concis et flexible que Java. Il apporte des fonctions de data-sciences.

- pandas
- sqlAlchemy
- Scikit-learn

::::
:::: column

Les ETL programmatiques :

- permet de factoriser des fonctionnalités
- outils sur mesure
- automatisation des déploiements
- testabilité des traitements
- demande davantage de ressources humaines
- long terme

::::
:::

# Orchestration

## Crontab

Crontab est un outil Linux permettant d'automatiser des commandes.

```
*    *    *   *    *  Command_to_execute
|    |    |    |   |
|    |    |    |    Day of the Week ( 0 - 6 ) ( Sunday = 0 )
|    |    |    Month ( 1 - 12 )
|    |    Day of Month ( 1 - 31 )
|    Hour ( 0 - 23 )
Min ( 0 - 59 )
```

---

Des examples de configuration crontab :

::: columns
:::: column

```
* * * * * sample.sh
```

. . .

Lance sample.sh toutes les minutes

. . .


```
*/10 * * * * sample.sh
```

. . .

Lance sample.sh toutes les 10 minutes


. . .

::::
:::: column

```
0 4,17 * * sun,mon sample.sh
```

. . .

Lance sample.sh à 4 et 17h le dimanche et lundi

. . .

```
30 07,09,13 * * * sample.sh
```

. . .

Lance sample.sh à 07:30, 09:30 et 13:30

::::
:::

## Makefile

### Make

Make est un programme linux permettant de lancer des tâches. Il peut être utilisé pour gérer des traitements de données.

---

```
extract:
        python extract.py --output sales.csv --start ${START_DATE} --end ${END_DATE}
transform:
        python transform.py --input sales.csv --output transformed.csv
load:
        python load.py --input transformed.csv --database datawarehouse --table sales
clean:
        rm  sales.csv transformed.csv
user_etl:
        # run an other makefile
        cd user && $(MAKE)
sales: clean extract transform load
```
Usage pour lancer l'ETL sur `user` puis sur `sales` :

```
make user_etl sales
```

---

Limitation des outils basiques de linux (makefile et crontab) :

- pas de centralisation (chaque serveur a son crontab)
- pas de visualisation sous forme de calendrier
- informations dans des fichiers
- pas d'alerte par mail
- pas de gestion de logs
- non adapté à de nombreux traitements (spaghetti code)
- dépendance et tâches conditionnelles complexes à implémenter


## Apache Airflow
### Airflow

Outil d'orchestration des pipelines de data-ingénierie.

C'est un outil avec une architecture 3 tiers :

1. interface utilisateur : permet l'affichage du calendrier, des opérations, des logs d'erreurs
2. serveur applicatif : permet le traitement des opérations (ETL/ELT)
3. base de données : stocke l'état des opérations

---

![Architecture d'Airflow](img/dwh-arch-airflow.png){width=60%}

---

Les opérations suivent l'ordre des flèches :

![Orchestration des dépendances Airflow](img/dwh-airflow-dag.png){width=100%}

---

Il peut y avoir des parcours d'arbres conditionnels :

![Orchestration conditionnelle Airflow](img/dwh-airflow-branching.png){width=80%}

---

Un exemple d'arbre d'operations airflow (DAG) :

```python
with DAG("my_daily_dag", email="admin@example.com", schedule_interval="0 * * * *"):
    first_task = EmailSensor(...)
    second_task = PythonOperator(...)
    third_task = SqlOperator(...)
    fourth_task = BashOperator(...)
    first_task >> [second_task, third_task]
    third_task << fourth_task
```

---

![Vue web Airflow](img/dwh-airflow-web.png){width=80%}

---

Conclusion sur airflow :

- `+` intègre crontab
- `+` dépendances plus avancées que make
- `+` gestion des emails, logs
- `+` visualisation des traitements de données
- `+` execute du code programmatique python / SQL / exécutables externes
- `+` sécurité et gestion des droits pour des équipes
- `-` pas pensé pour les traitements streaming
- `-` inadapté aux traitements de machine learning

# La qualité

## La qualité des données

### DQ

La Data-Quality, est un processus essentiel à la confiance dans les informations. Il peut être automatisé ou réalisé à
la demande. Il s'agit d'étudier les données sur différents plans :

- au niveau individuel : des contraintes d'intégrité (cf cours 1)
- au niveau global : des mesures quantitatives globales (moyennes, max, min, count...)
- au niveau relatif : en comparant les données à des moments différents, on peut déceler des erreurs dans les ETL
- des règles combinant tous les niveaux


Exemples d'outils :

- Great Expectations (python)
- Deequ (java)
- Talend Data-Quality (outil graphique)


## L'observabilité
### Le monitoring
Le bon fonctionnement du matériel est un préalable à celui des traitements et des données.

Exemples d'outils :

- prometheus/grafana
- ELK (Elastic Logstash Kibana)
- datadog (propriétaire)

---

![Monitoring avec prometheus](img/dwh-prometheus.png){width=85%}

# Le datalake

Le data-warehouse basés sur les systèmes MPP ne sont pas toujours suffisants pour gérer les 3V du big-data :

- le volume : les coûts de stockages sont incompatibles avec la gestion de peta-octets
- la variété : les données semi-structurées (json, xml, html, logs) et non structurées (images, vidéo, son, signal)
- la vélocité : l'acquisition de données en temps réel n'est pas compatible avec les fonctionnements "batch"

---

Les datalakes sont rendus possibles grâce aux capacités de stockage inédites : l'accroissement du volume est peu
couteux, les performances en lecture/écritures (IO) très bonnes et la redondance est assurée.

### HDFS
Les premiers datalakes ont été possibles sur hadoop grâce au système de fichier immutable HDFS.

### Le stockage S3

S3 (Simple Storage Service) est un système de fichier "objet". Comme le web, la gestion S3 est faite via des url et des
verbes GET, PUT, DELETE.

---

Les usages du datalake :

### Stockage/Analyse des données brutes

Le datalake est utilisé comme dépôt de données de l'entreprise. L'ensemble de l'historique y est consolidé et permet de
répondre à des questions qui pourront être posées a posteriori, sur des données brutes. (contrairement au data-warehouse
qui ne peut pas toujours encaisser les 3V).

### Stockage/Analyse des données transformées

Les systèmes New-SQL (cf cours 3) offrent des capacités de calculs et une élasticité supérieures aux MPP. L'émergence
des solutions ACID sur le format parquet permet les **datalake-house** : un data-warehouse construit sur le datalake.

Les datalake-house sont plus complexes que les data-warehouse mais l'infrastructure est plus accessible pour des gros
volumes. (coût ingénieur ++ / coût infrastructure --)

---

![](img/dwh-lakehouse.png){width=102%}

# Le cloud

Les différentes options pour mettre en œuvre un projet informatique :

::: columns
:::: column

### On-Premise
La gestion se fait entièrement sur site par les employés. (non Cloud)

### IaaS
Le matériel n'est plus géré sur site (dans le cloud).

::::
:::: column

### PaaS
Le matériel et le logiciel applicatif sont gérés par les employés du cloud.

### SaaS
Les employés sur site ont accès à des logiciels entièrement gérés par ceux du cloud.

::::
:::

---

![Les différents niveaux de gestion](img/dwh-cloud.png){width=85%}


---

On distingue :

- cloud privé : l'infrastructure est privatisée pour un seul client
- cloud public : l'infrastructure est partagée par différents clients

### Les cloud public extraterritoriaux

- Amazon Web Services (AWS)
- Google Cloud Platform (GCP)
- Azure (Microsoft)
- Alibaba Cloud

### Les cloud public francais

- OVH (On Vous Héberge)
- Scaleway
- Outscale

---

### Le Cloud Act

Adopté en mars 2018, cette loi extraterritoriale américaine permet aux administrations des États-Unis, disposant d’un
mandat et de l'autorisation d’un juge, d'accéder aux données hébergées dans les serveurs informatiques des sociétés
américaines ou ayant une activité aux USA.

# Le Streaming

### Le Stream

Le traitement batch ne permet pas le temps réel. Hors dans de nombreux cas, la fraicheur de l'information est cruciale.

### Les Event Log


::: columns
:::: column

![Event Log](img/dwh-event-log.png){width=100%}

::::
:::: column

Les event log permettent d'empiler l'information pour la transmettre à plusieurs consommateurs.

Exemple d'outils :

- apache kafka
- apache pulsar
- rabbitMQ

::::
:::

## Le stream processing


::: columns
:::: column

![Stream Processing](img/dwh-stream.png){width=70%}

::::
:::: column

Le stream processing permet de consommer un event log, de le transformer et d'être à son tour consommé.

Exemple d'outils :

- apache storm
- apache kafka streaming
- apache spark streaming

::::
:::

---

### Évolution

Un des enjeux de la gestion des données de stream, est de faire évoluer le rendu (la table d'arrivé). Par exemple, en
cas d'amélioration des transformations, ou d'ajout de KPI. Deux architectures simplifient cette évolution :

- architecture lambda : deux outils transforment les données, batch et stream. Une nouvelle version des deux systèmes
  permet de faire évoluer le rendu
- architecture kappa : un seul outil réalise les transformations. Une nouvelle version du job permet de faire évoluer le
  rendu

## L'architecture Lambda

![Architecture Lambda](img/dwh-arch-lambda.png){width=80%}

## L'architecture Kappa

![Architecture Kappa](img/dwh-arch-kappa.png){width=80%}

# La restitution
## La BI

### Business Intelligence

Le business Intelligence (ou décisionnelle) répond aux besoins de reporting et de data visualisation.

Les outils s'appuient en général sur des datamarts pour puiser les données (tout comme le cube OLAP).

Dans l'entreprise, les dashboard s'adressent à tous les niveaux : décideurs, chefs de projets, developeurs.

---

Outils Propriétaires :

- Tableau
- SAP Business Object
- IBM Cognos
- Microsoft Power BI

Outils FOSS (Free-Open Source Software) :

- apache superset (python)
- metabase (java)

---

![Apache Superset](img/dwh-superset.png){width=80%}

---

![Metabase](img/dwh-metabase.png){width=80%}

## Les API

### API
L'information des datawarehouse peut être valorisée par des API consommables comme des bases opérationnelles.

On utilise alors des bases de données de type OLTP, consommées par des backend.

![**RE**presentational **S**tate **T**ransfer](img/dwh-apiRest.png){width=70%}

---

### Les Feature Store

C'est un certain type d'API qui fournit des données dérivées, enrichies souvent par des algorithmes de data-mining ou
d'apprentissage machine.

Les "features" peuvent être consommées pour des traitements innovants.

# Références

- Business Intelligence, Analytics and Data Science (2018) - Ramesh Sharda
- I hearth logs (2014) - Jay Kreps
- The Data Warehouse Toolkit (2013) - Ralph Kimball
- Building the Data Warehouse (2005) - Bill Inmon
- Building the Operational Data Store (1996) - Bill Inmon
