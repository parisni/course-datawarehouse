# Master 2 CPI, PARIS 8 - Plateforme Data

## Examen - Rapport individuel à Réaliser

Suite à votre travail remarquable de chef de projet informatique, la société
dans laquelle vous êtes apprenti cette année vous confie la mission de mettre
en place une plateforme data. Par chance, vous avez eu un cours sur ce sujet à
l'Université et vous décidez de réaliser un rapport afin de justifier un budget
significatif, et de montrer votre maîtrise du sujet à votre hierarchie.

Vous traiterez des aspects suivants :

- introduction sur les objectifs de l'entreprise
- que peut apporter une plateforme data
- quelles sont les données collectées/générées par l'entreprises
- un schéma du modèle physique de données de l'entreprise (les sources)
- un schéma du modèle physique de données du datawarehouse
- la technologie utilisée pour créer le datawarehouse (type de base de données, le type d'ETL)
- aurez vous besoin de bases de données spécialisées (graphe, clé valeur, document...) ?
- aurez vous besoin de mettre en place un datalake ?
- choisirez vous le cloud et pourquoi ?
- les composants de votre plateforme data (avec un schéma d'architecture complet, reprenant les sources de données, le datawarehouse, les flux ETL etc...)
- un organigramme de l'équipe nécessaire
- une timeline de la mise en œuvre du projet, et une estimation des couts

Remarques:

- chaque décision sera justifiée, si possible en utilisant le cours
- si votre entreprise a déjà une telle plateforme data l'objectif n'est pas de la
décrire dans le rapport, mais de proposer votre propre projet
- si votre entreprise n'est pas adaptée au sujet, vous pouvez inventer une
entreprise fictive

Du point de vue pratique:

- Il compte pour 50% de la note finale
- chatgpt sera sanctioné
- travail individuel (idem)
- 5 à 8 pages
- retourner à: <nicolas.paris@riseup.net>
- sujet: [M2-CPI] Rapport - Nom / Prénom
