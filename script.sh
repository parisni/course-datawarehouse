#!/usr/bin/env sh

SOURCE=$1
DATE_COVER=$(date "+%d %B %Y")

SOURCE_FORMAT="markdown_strict\
+pipe_tables\
+backtick_code_blocks\
+auto_identifiers\
+strikeout\
+yaml_metadata_block\
+implicit_figures\
+all_symbols_escapable\
+link_attributes\
+smart\
+fenced_divs"

DATA_DIR="pandoc"

pandoc -s --dpi=96 --slide-level 2 --toc --listings  --data-dir="${DATA_DIR}" --template default_mod.latex -H pre.tex --pdf-engine xelatex -f "$SOURCE_FORMAT" -M date="$DATE_COVER" -V classoption:aspectratio=169 -t beamer $SOURCE.md -o $SOURCE.pdf
