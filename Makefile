# begin trick
# allows passing args to make file
# see https://stackoverflow.com/a/47008498/3865083
# use below command to get the args:
# $(filter-out $@,$(MAKECMDGOALS))
%:
        @:
# end trick

docker_cmd:=docker run -it --rm -v ./:/course  course ./script.sh
.PHONY: 1_sgbdr
1_sgbdr:
	${docker_cmd} 1_sgbdr

.PHONY: 2_nosql
2_nosql:
	${docker_cmd} 2_nosql

.PHONY: 3_olap
3_olap:
	${docker_cmd} 3_olap

.PHONY: 4_dwh
4_dwh:
	${docker_cmd} 4_dwh

.PHONY: evaluation
evaluation:
	docker run -it --rm -v ./:/course  course ./evaluation.sh
