<!--
 see https://github.com/alexeygumirov/pandoc-beamer-how-to
 -->
---
title: "Les bases de données relationnelles"
author: "Nicolas Paris"
institute: "Université Parix VIII"
topic: "Entrepôts de Données"
theme: "CambridgeUS"
colortheme: "crane"
fonttheme: "professionalfonts"
outertheme: infolines
fontsize: 11pt
urlcolor: red
linkstyle: bold
aspectratio: 169
date-course: 11/10/2024
section-titles: false
toc-title: Plan du cours
toc: true
---

# Historique
La première base de données informatisée était probablement un **fichier**


. . .

Exemple de base de données clé / valeur en deux lignes:

```bash
#!/bin/bash
db_set () {
echo "$1,$2" >> database
}
	db_get () {
grep "^$1," database | sed -e "s/^$1,//" | tail -n 1
}
```
. . .

::: columns
:::: column
- `+` ajouter en temps  constant: `O(1)`
- `-` récupérer, fonction de la taille de la base: `O(n)`
- `+` données variées dans valeur
::::
:::: column
- `-` pas moyen de valider les données
- `-` corruption si écritures concurrentes
- `-` mise à jour impossible
::::
:::


## Les données tabulaires
![Des problèmes ?](img/excel-1.png){width=70%}

---

- casse différentes pour les prénoms
- patientes différentes avec le même prénom
- modalités de services identiques codés différemment


# Le modèle relationnel
### Bio
En 1970, Edgar Frank Codd a posé les bases du modèle relationnel :

1. abstraire le stockage des données sur le disque
2. fournir une base solide pour la cohérence et la redondance des données
3. permettre le développement de langages de manipulation de données
4. être un modèle extensible de données tabulaires pouvant manipuler des données complexes
5. devenir un standard pour la gestion des données

### Apports
- règles relationnelles
- concepts relationnels
- cardinalités
- algèbre relationnel

---

![Le père fondateur](img/codd.png){width=65%}

## Les 12 règles relationnelles

::: columns
:::: column
- Unicité
- Garantie d'accès
- traitement des valeurs nulles
- catalogue lui même relationnel
- sous langage de données
- mise à jour des vues

::::

:::: column
- insertion, mise à jour et effacement de haut niveau
- indépendance physique
- indépendance logique
- indépendance d'intégrité
- indépendance de distribution
- règle de non-subversion
::::
:::

## Les concepts relationnels

![Les concepts relationnels](img/modele-r.png){width=80%}

---

::: columns
:::: column

### Tuple
Enregistrement ou ligne dans la base de données

### Attribut
Valeur associée à l'un des élément du tuple. Autrement dit, la colonne.

### Relation
Ensemble d'attributs et de tuples formant une table. Ne pas confondre
avec les relations entre tables.


::::
:::: column
### Domaine
L'ensemble fini des valeurs possibles pour un attribut donné

### Degré
Nombre d'attributs dans une relation

::::
:::


## Les cardinalités
::: columns
:::: column
### Definition
Les cardinalités représentent l'articulation entre les
relations. Elles se matérialisent par des clés primaires/étrangères
voire une table d'association pour les cardinalités N:N.

::::
:::: column
- **1:1**: un tuple de la relation A se rapporte a un seul tuple de la relation B
- **1:N**: un tuple de A se rapporte à plusieurs tuples de B
- **N:N**: un tuple de A se rapporte à plusieurs tuples de B et __vice-versa__

::::
:::
## L'algèbre relationnel
::: columns

:::: column
### Définition
Transforme une ou plusieurs relations en entrée, en une seule relation
en sortie.

::::

:::: column

Il existe 3 types d'opérations :

- unaires : accepte une relation
- binaires : accepte deux relations
- n-aires ou ensemblistes: accepte n relations

::::
:::

---

### Les opérations unaires
- selection
- projection
- renommage

. . .

### Les opérations binaires
- jointure
- division

. . .

### Les opération n-aires
- union
- intersection
- difference
- produit carthesien


---

![Le produit carthesien](img/cart-prod.png){width=75%}

# Le langage SQL
### Histoire
Le SQL (Structured Query Language) ou ISO/CEI 9075 apparait
en 1975. Nous prendrons comme exemple PostgreSQL qui est le SGBDR au
plus proche du standard.

. . .

::: columns
:::: column

### Types de requètes
- définition de données
- manipulation de données
- contrôle des données
- contrôle des transactions

. . .

::::

:::: column
### Façons d'utiliser le SQL:

- interactive
- insérée dans un programme
- procédures stockées

. . .

::::
:::

### Conventions des commandes
La syntaxe utilisée est l'"Unix manual-page-style textual".
Les crochets `[]` indiquent du code optionnel. Des accolades `{}` et
des tubes `|`, indiquent un choix. Points de suspension `...`
indiquent une répétition possible du code.




## Les types
::: columns
:::: column

### Les types d'objets
- tables
- colonnes
- types
- contraintes d'intégrité
- indexes
- vues logiques
- vues materialisées
- partitions
- fonctions
- procédures stocquées
- triggers

::::
:::: column
### Les types de colonnes
- int32
- int64
- float
- double
- varchar(n)
- text
- enum
- array
- json
- xml
- ...

::::
:::

## Commande Create Table
```
CREATE [ { TEMPORARY | TEMP } | UNLOGGED ] TABLE [ IF NOT EXISTS ] table_name ( [
  { column_name data_type [ COMPRESSION compression_method ] [ COLLATE collation ] [ column_constraint [ ... ] ]
    | table_constraint
    | LIKE source_table [ like_option ... ] }
] )

where column_constraint is:

[ CONSTRAINT constraint_name ]
{ NOT NULL |
  NULL |
  CHECK ( expression ) [ NO INHERIT ] |
  DEFAULT default_expr |
  UNIQUE index_parameters |
  PRIMARY KEY index_parameters |
  REFERENCES reftable [ ( refcolumn ) ] [ MATCH FULL | MATCH PARTIAL | MATCH SIMPLE ]
    [ ON DELETE referential_action ] [ ON UPDATE referential_action ] }
[ DEFERRABLE | NOT DEFERRABLE ] [ INITIALLY DEFERRED | INITIALLY IMMEDIATE ]

```
## Example Create Table

```sql
CREATE TABLE films (
    code        char(5) CONSTRAINT firstkey PRIMARY KEY,
    title       varchar(40) NOT NULL,
    did         integer NOT NULL,
    date_prod   date,
    kind        varchar(10)
);

```

```sql
CREATE TABLE distributors (
    did     integer,
    name    varchar(40),
    CONSTRAINT con1 CHECK (did > 100 AND name <> '')
);
```

## Commande Select (1)
```
[ WITH [ RECURSIVE ] with_query [, ...] ]
SELECT [ ALL | DISTINCT [ ON ( expression [, ...] ) ] ]
    [ * | expression [ [ AS ] output_name ] [, ...] ]
    [ FROM from_item [, ...] ]
    [ WHERE condition ]
    [ GROUP BY [ ALL | DISTINCT ] grouping_element [, ...] ]
    [ HAVING condition ]
    [ WINDOW window_name AS ( window_definition ) [, ...] ]
    [ { UNION | INTERSECT | EXCEPT } [ ALL | DISTINCT ] select ]
    [ ORDER BY expression [ ASC | DESC | USING operator ] [ NULLS { FIRST | LAST } ] [, ...] ]
    [ LIMIT { count | ALL } ]
    [ OFFSET start [ ROW | ROWS ] ]
    [ FETCH { FIRST | NEXT } [ count ] { ROW | ROWS } { ONLY | WITH TIES } ]
    [ FOR { UPDATE | NO KEY UPDATE | SHARE | KEY SHARE } [ OF table_name [, ...] ] [ NOWAIT | SKIP LOCKED ] [...] ]
```

## Commande Select (2)
```
where from_item can be one of:

    [ ONLY ] table_name [ * ] [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
                [ TABLESAMPLE sampling_method ( argument [, ...] ) [ REPEATABLE ( seed ) ] ]
    [ LATERAL ] ( select ) [ AS ] alias [ ( column_alias [, ...] ) ]
    with_query_name [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
    [ LATERAL ] function_name ( [ argument [, ...] ] )
                [ WITH ORDINALITY ] [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
    [ LATERAL ] function_name ( [ argument [, ...] ] ) [ AS ] alias ( column_definition [, ...] )
    [ LATERAL ] function_name ( [ argument [, ...] ] ) AS ( column_definition [, ...] )
    [ LATERAL ] ROWS FROM( function_name ( [ argument [, ...] ] ) [ AS ( column_definition [, ...] ) ] [, ...] )
                [ WITH ORDINALITY ] [ [ AS ] alias [ ( column_alias [, ...] ) ] ]
    from_item [ NATURAL ] join_type from_item [ ON join_condition | USING ( join_column [, ...] ) [ AS join_using_alias ] ]
```

## Commande Select (3)
```
and grouping_element can be one of:

    ( )
    expression
    ( expression [, ...] )
    ROLLUP ( { expression | ( expression [, ...] ) } [, ...] )
    CUBE ( { expression | ( expression [, ...] ) } [, ...] )
    GROUPING SETS ( grouping_element [, ...] )

and with_query is:

    with_query_name [ ( column_name [, ...] ) ] AS [ [ NOT ] MATERIALIZED ] ( select | values | insert | update | delete )
        [ SEARCH { BREADTH | DEPTH } FIRST BY column_name [, ...] SET search_seq_col_name ]
        [ CYCLE column_name [, ...] SET cycle_mark_col_name [ TO cycle_mark_value DEFAULT cycle_mark_default ] USING cycle_path_col_name ]

TABLE [ ONLY ] table_name [ * ]
```

## Example Select (1)
Union:
```sql
WITH t AS (
    SELECT random() as x FROM generate_series(1, 3)
  )
SELECT * FROM t
UNION ALL
SELECT * FROM t;
```
Aggregation:
```sql
SELECT kind, sum(len) AS total
    FROM films
    GROUP BY kind
    HAVING sum(len) < interval '5 hours';
```
## Example Select (2)



Recursivité:
```sql
WITH RECURSIVE employee_recursive(distance, employee_name, manager_name) AS (
    SELECT 1, employee_name, manager_name
    FROM employee
    WHERE manager_name = 'Mary'
  UNION ALL
    SELECT er.distance + 1, e.employee_name, e.manager_name
    FROM employee_recursive er, employee e
    WHERE er.employee_name = e.manager_name
  )
SELECT distance, employee_name FROM employee_recursive;
```
### Principe
Une condition initiale, suivie par une UNION, suivie par la partie recursive de la commande.

## Les joins
![](img/join.png){width=72%}
## Commande Update
```
UPDATE [ ONLY ] table_name [ * ] [ [ AS ] alias ]
    SET { column_name = { expression | DEFAULT } |
          ( column_name [, ...] ) = [ ROW ] ( { expression | DEFAULT } [, ...] ) |
          ( column_name [, ...] ) = ( sub-SELECT )
        } [, ...]
    [ FROM from_item [, ...] ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING * | output_expression [ [ AS ] output_name ] [, ...] ]
```
Example:
```sql
UPDATE films SET kind = 'Dramatic' WHERE kind = 'Drama';
UPDATE employees SET sales_count = sales_count + 1 FROM accounts
  WHERE accounts.name = 'Acme Corporation'
  AND employees.id = accounts.sales_person;
```

## Commande Delete
```
[ WITH [ RECURSIVE ] with_query [, ...] ]
DELETE FROM [ ONLY ] table_name [ * ] [ [ AS ] alias ]
    [ USING from_item [, ...] ]
    [ WHERE condition | WHERE CURRENT OF cursor_name ]
    [ RETURNING * | output_expression [ [ AS ] output_name ] [, ...] ]
```
Examples:
```sql
DELETE FROM films
  WHERE producer_id IN (SELECT id FROM producers WHERE name = 'foo');
DELETE FROM tasks WHERE status = 'DONE' RETURNING *;
```

## Commande transaction
Débuter un bloc de transaction:
```sql
BEGIN [ WORK | TRANSACTION ] [ mode_transaction [, ...] ]

où mode_transaction peut être :

    ISOLATION LEVEL { SERIALIZABLE | REPEATABLE READ | READ COMMITTED | READ UNCOMMITTED }
```



---

Par suite:

- Enchainement de n'importe quelle commande (`create table`, `delete`, `select`....)
- **ROLLBACK** — annule la transaction en cours et toutes les modifications effectuées lors de cette transaction.
- **COMMIT** — valide la transaction en cours


```sql
BEGIN;
create table kv_store {key string primary key, value json};
insert into kv_store (key, value) VALUES ('9307cf29', '{"hello": "world"}')
ROLLBACK;
-- la transaction est annulée, la table n'existe pas...
```



## Les procédures stockées
Creation de procedure stoquée:
```
CREATE [ OR REPLACE ] PROCEDURE
    name ( [ [ argmode ] [ argname ] argtype [ { DEFAULT | = } default_expr ] [, ...] ] )
  { LANGUAGE lang_name
    | AS 'definition'
  } ...

```

Avec **lang\_name** dans: sql, python3, c, tcl, perl, java, R, javascript, lua et sh

Pour appeler une procedure stockée:
```
CALL name ( [ argument ] [, ...] )
```

---

Bilan sur les procédures stockées:

- `+` évite des échanges réseaux entre plusieurs requètes
- `+` évite de parser le sql à chaque appel (parsé lors de l'ajout de la fonction)
- `+` les résultats intermédiaires inutiles restent ne transitent pas
- `-` gestion du code source des procédure compliqué (pas d'IDE, gestion des déploiements...)


---

Example de PL-python3

```pgsql
CREATE FUNCTION pymax (a integer, b integer)
  RETURNS integer
AS $$
  if a > b:
    return a
  return b
$$ LANGUAGE plpythonu;
```
## Les vues
### Vues logiques
Une requête SQL de type **SELECT** peut être utilisée pour créer une
table dite "vue". On peut lire cette vue, ce qui déclenche la
requête. On peut aussi réaliser des `update` et `delete`, ce qui
impacte les tables physiques sources de la vue.

- `+`: pas besoin de redonder les données
- `+`: fraicheur des données
- `-`: calculs couteux en fonction de la requête

---

### Vues materialisées
De même que la vue logique, une requête **SELECT** peut être utilisée
pour créer une table dite "vue matérialisée", sauf qu'elle n'est
calculée qu'une fois. Cette vue est recalculée à la demande. Une mise
à jour automatique est trop complexe.

- `+`: meilleures performances que les vues logiques
- `+`: peuvent être indexées
- `-`: mauvaise fraicheur des données

# Le fonctionnement d'un SGBDR
## Les WAL
### Journalisation avant écriture (Write Ahead Log)
Fichier système contenant d’une part les valeurs (images) avant
modification des pages mises à jour, dans l’ordre des modifications
avec les identifiants des transactions modifiantes, ainsi que des
enregistrements indiquant les débuts, validation et annulation de
transactions.

---

![Journal avant écriture](img/wal.png){ width=75%}

## Les fichiers
```sql
CREATE DATABASE test;
CREATE TABLE test_table my_id serial , mytext varchar;
INSERT INTO test_table (mytext) VALUES ('hello there');
SELECT oid as object_id, datname FROM pg_database where datname ='test';
object_id | datname
-----------+---------------
20886 | test
SELECT pg_relation_filepath('test_table');
pg_relation_filepath
----------------------
base/20886/186770

hexdump -C /database/master/base/20886/186770
00000000 a8 00 00 00 d0 2a d7 55 00 00 00 00 1c 00 d8 1f |.....*.U........|
00000010 00 20 04 20 00 00 00 00 d8 9f 50 00 00 00 00 00 |. . ......P.....|
00000020 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
00001fd0 00 00 00 00 00 00 00 00 99 30 5b 02 00 00 00 00 |.........0[.....|
00001fe0 00 00 00 00 00 00 00 00 01 00 02 00 02 09 18 00 |................|
00001ff0 01 00 00 00 19 68 65 6c 6c 6f 20 74 68 65 72 65 |.....hello there|
```

---

### Fonctionnement
Les SGBDR stockent les objects (base, tables, indexes, wal...) dans des
fichiers sur l'OS, dans des structures de données optimisées et sûres


## Stockage en ligne
Le SGBR est optimal pour récupérer rapidement toutes les infos d'une même ligne.

![Toutes la ligne au même endroit](img/table-row.png){width=80%}

# Optimisations
## Les full scan
### Principe
Lorsqu'on réalise une operation relationnelle de **selection** sur une
table (un filtre **WHERE**), le moteur SGBDR peut tester les valeurs du
**prédicat** de chaque ligne. Il doit lire toutes les données sur le
disque. L'autre option est d'utiliser **l'indexation**.

### Ne pas confondre
La close `select` du langage sql et la `selection` qui est une
opération unaire d'algèbre relationnel.

## Fonctionnement des disques mécaniques
::: columns
:::: column
![disque mécanique](img/disque_meca.png)
::::
::::column
* `+` bas coût
* `+` bonnes performances de lecture sequentielles
* `-` latences de lectures aléatoires (déplacement des têtes de lectures)
::::
:::

## Fonctionnement des disques ssd


### Principe
Disques purement electroniques. Interfaces PCIe, NVME

* `+` excellentes performances de lectures/écritures sequentielles/aléatoires
* `-` coût élevés
* `-` amplification à la ré-écriture


## L'indexation
Revenons sur la base de donnée fichier:

```bash
#!/bin/bash
db_set () {
echo "$1,$2" >> database
}
db_get () {
grep "^$1," database | sed -e "s/^$1,//" | tail -n 1
}
```
- Les temps d'ajout de valeur est constant: `O(1)`.
- Le temps de récupération d'une valeur est fonction de la taille de la base: `O(n)`.

---

### Objectif
Pour éviter d'avoir a scanner toute la base, l'indexe est une
structure suplémentaire à maintenir. L'acceleration de l'accès aux
données avec pour contrepartie, l'augmentation du cout de maintenance.

## L'indexation b-tree
On construit un arbre stocké sur disque, chaque branche contient n
références ordonnées vers d'autres branches ou des des feuilles qui
pointent vers les valeurs.

Pour un arbre de 4 niveaux de profondeur avec un facteur de
branchement de 500, peut stocker 256TB.

---

![Indexes avec facteur de branchement=5](img/btree.png){ width=75% }

---

L'indexe b-tree:

- `+` accés trés efficace aux données, connaissant la clé `O(log n)`
- `+` résilient aux crash grâce aux WAL
- `-` volumineux
- `-` coût de mise à jour ou d'ajout de tuples
- `-` inefficace pour récupérer de nombreuses clé (> 10% du nombre de
  lignes); un scan séquentiel ou un index bloom est alors plus
  efficace.

## L'indexation bloom
Vérifie la présence d'un élément dans un set, via une structure de
données probalistique très compacte, mais avec des faux positifs (sans
faux négatifs).

- `+` permet d'éviter les scan séquentiels sur des grosses tables
- `+` taille de l'indexe très compacte
- `-` Seul l'opération d'égalité est supportée par cet index
- `-` temps d'accès de l'ordre de la secondes
- `-` complexe à optimiser (rapport taille/rappel)

## L'indexation hash
Utilise une structure de hash map. Pour chaque valeur d'une colonne,
on a une clé de hashmap (entier 2^32). La valeur du hashmap est la
liste des lignes contenant cette clé.

- `+` peux volumineux comparé aux b-tree
- `-` il faut recheck les lignes, en cas de collision de hash

## L'indexation bitmap

Stocker un bitmap pour chaque valeur de clé.

![Bitmap](img/bitmap.png){ width=50% }

---

L'indexe bitmap:

- `+` accélère la selectivité sur colonne dont les sont comprises entre 100 et 100k
- `+` taille d'indexe réduite comparativement aux b-tree
- `-` contre productif pour d'autres modalités
- `-` inadapté aux colonnes dont la valeur change régulièrement

## L'indexation trgm
Indexe les champs textuels, en explosant les mots en trigrammes. Par
exemple, `cat` contient les trigrammes `c`, `ca`, `at` et `t`. Si on
recherche `%at` alors on obtient `at` et le tuple sera trouvé dans
l'indexe. Enfin, un recheck permet d'eliminer les faux positifs (comme
`cats`).

- `+` gère les expressions régulières
- `+` permet de chercher par `pattern` de début/fin
- `-` volumineux
- `-` pas adapté aux textes longs (trops de trigrammes)

## L'indexation full-text

::: columns
:::: column

### Tokeniser
Sépare les mots. Par exemple: `grand-père` est un seul token, mais `est-il` deux tokens.

### Mots stops
Les mots trop répandus et les ponctuations sont retirés.

::::
:::: column

### Synonyms
Un dictionnaire de synonymes, permet de remplacer par un equivalent
unique.

### Stemming
Pour tout mot, remplace par sa racine. L'algorithme snowball est
disponible dans plusieurs langues.

::::
:::

---

L'indexation full-text:

- `+` permet de faire un moteur de recherche
- `+` enchainement de mots, phrases
- `-` volumineux et lent à indexer
- `-` inefficace lorsque la condition de filtre est peu selective

## Les plans d'execution
Ils permettent de comprendre comment les requêtes sont traitées, pour
les optimiser.
### Cost
Représentation du temps.
**cost=startup..total**: __startup__ est le temps au démarrage, et
__total__ est l'estimation de la fin.

### Rows
Estimation de nombre de lignes retournées par chaque étapes.

### Width
Représentation de la taille moyenne des lignes retournées en
bytes. Plus précis que le nombre de colonnes.

---

Posgresql collecte des statistiques sur chaque tables, et chaque
champs, et les utilise dans ses plans d'exécutions.

```sql
ANALYSE devices;
select attname, null_frac, avg_width, n_distinct, correlation
from pg_stats where tablename = 'devices' limit 10;
+--------------+-------------+-----------+-------------+---------------+
|   attname    |  null_frac  | avg_width | n_distinct  |  correlation  |
+--------------+-------------+-----------+-------------+---------------+
| device_id    |           0 |        27 |          -1 |  -0.020291287 |
| hidden       |           0 |         1 |           2 |     0.2228249 |
| user_agent   |  0.49579832 |       101 | -0.32212886 |   -0.20333344 |
| user_id      |           0 |        31 |  -0.1904762 |   0.110935606 |
| last_seen    |  0.49579832 |         8 | -0.50420165 |      0.845273 |
| display_name | 0.005602241 |        24 | -0.11764706 | -0.0061968076 |
| ip           |  0.49579832 |        14 | -0.31092438 |    0.06324063 |
+--------------+-------------+-----------+-------------+---------------+
```

---

Scan sequentiel:
```sql
EXPLAIN SELECT * FROM tenk1 WHERE unique1 < 7000;

                         QUERY PLAN
------------------------------------------------------------
 Seq Scan on tenk1  (cost=0.00..483.00 rows=7001 width=244)
   Filter: (unique1 < 7000)
```

Utilisation d'indexe:
```sql
EXPLAIN SELECT * FROM tenk1 WHERE unique1 = 42;

                                 QUERY PLAN
-----------------------------------------------------------------------------
 Index Scan using tenk1_unique1 on tenk1  (cost=0.29..8.30 rows=1 width=244)
   Index Cond: (unique1 = 42)
```

---

Plan imbriqué:
```sql
EXPLAIN(
  SELECT *
  FROM chat_room_messages
  WHERE author IN (
    SELECT email FROM users WHERE updated_at > '2021-01-01'::date
  )
);
                                 QUERY PLAN
-----------------------------------------------------------------------------
 Hash Join  (cost=10.84..22.49 rows=17 width=580)
   Hash Cond: ((chat_room_messages.author)::text = (users.email)::text)
   ->  Seq Scan on chat_room_messages  (cost=0.00..11.30 rows=130 width=580)
   ->  Hash  (cost=10.62..10.62 rows=17 width=516)
         ->  Seq Scan on users  (cost=0.00..10.62 rows=17 width=516)
               Filter: (updated_at > '2021-01-01'::date)
```

## Le sharding
Lorsqu'on atteint les limites physiques d'un serveur (RAM, CPU), on a deux options:

1. augmenter la RAM, CPU
2. ajouter un serveur (= sharding)

Les SGBDR ont été conçu pour fonctionner sur un seul serveur. Il
existe cependant de nombreuses méthodes pour bénéficier de plusieurs
serveurs. Par exemple:

- le partitionnement horizontal
- le hot standby

## Partitionnement horizontal

### Definition
Consiste à répartir les données d'une table sur plusieurs machines, en
se basant sur une clé de partitionnement. Les écritures/lectures
seront effectuées directement sur la machine, en fonction de cette clé.


### Exemple
Une table **sales** avec la colonne **country** pourra avoir une base
par pays pour répartir les données de cette table. Les autres tables,
pourront ou non être partitionnées de la sorte.

## Hot standby
### Rappel
Les WAL sont les journaux qui contiennent toutes les transaction. Ils
peuvent être envoyés (streaming replication) à d'autres serveurs pour
répliquer les données physiques.

### Hot standby
Un serveur **primaire** reçoit les écritures, et réplique ses WAL sur
n autres serveurs qui peuvent recevoir des requêtes en lecture
seule. Un **load balancer** peut ensuite répartir la charge de lecture
sur les n+1 serveurs.

- `+` on peut encaisser davantage de requêtes de type SELECT
- `-` on ne peut pas augmenter les capacités d'écriture
- `-` la réplication n'est pas instantanée, et deux clients peuvent
  voir un état différent

# Les transactions

## ACID
Atomicité, Consistence, Isolation & Durabilité est un concept né en
1983, pour décrire les systèmes transactionnels.


- A: ne peut pas être découpé en plusieurs morceaux
- C: la base est dans un bon état
- I: les processus concurrent son considérés comme séquentiels
- D: les données sont en lieu sûr

## Les enjeux transactionnels
### Dirty reads
Lorsqu'on lit depuis le SGBDR, on ne doit voir que ce qui a été commit.

![](img/dirty-read.png){width=75%}

Ici, pas de dirty read.

---

### Dirty writes
Lorsqu'on écrit dans le SGBDR, on ne dois écraser que ce qui a été commit.

![](img/dirty-write.png){width=75%}

Problème: c'est Bob qui paye et Alice qui reçoit.

## Les niveaux de transactions
### Implémentations ACID
En fonction du SGBDR et des besoins de l'application, différents
niveaux d'exigence sont possibles, avec leurs avantages et
inconvénients:

Read Uncommited < read committed < snapshot isolation < serializable

## Read committed
Pour éviter les problèmes de **dirty write**, on utilise un lock au
niveau de la table. Toute transaction voulant écrire dans la table
doit attendre.

Pour éviter le problèmes de **dirty read**, on met un lock, le temps
de la lecture. Cela pose des problèmes dans les cas:
- sauvegarde de la base
- longues requêtes de caculs

## Snapshot Isolation
De même que le précédent, les **dirty write** sont évités en mettant un
lock sur la table.

Pour éviter les **dirty read**, on conserve une version de la base avant
toute transaction d'écriture, qui sert à la lecture courante.

Ce mode transactionnel est aussi appelé Multi Version Concurrency
Control (MVCC).

## Serialisable

C'est le niveau le plus aboutit d'isolation. Chaque transaction est
traitée de manière séquentielle en n'utilisant qu'**un seul
processus**. La contrepartie est de perdre en **parallélisation** et
donc en performances.

# Contraintes d'intégrité
Les tables peuvent avoir des règles d'injestion de données:

- **check**: une expression booléenne donnée doit être satisfaite
- **unique**: unicité des valeurs sur une ou plusieurs colonne
- **not null**: toutes les lignes sont à renseigner pour cette colonne
- **primary key**: à la fois `unique` et `not null`
- **foreign key**: la (ou les) colonne, a des valeurs existante dans une autre table

---

```sql
CREATE TABLE products (
    product_id integer PRIMARY KEY,
	owner_id integer REFERENCES owners (product_id)
    name text NOT NULL,
	product_uid UNIQUE,
	comment text,
    price numeric CHECK (price > 0)
);
```
# L'administration du SGBDR
## Les droits d'accès
Un administrateur gère les utilisateurs et les accès aux objets du SGBDR:

```
GRANT { { SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER }
    [,...] | ALL [ PRIVILEGES ] }
    ON { [ TABLE ] table_name [, ...]
         | ALL TABLES IN SCHEMA schema_name [, ...] }
    TO { [ GROUP ] role_name | PUBLIC } [, ...] [ WITH GRANT OPTION ]
```
---

On peut gérer les droits au niveaux des colonnes:

```
GRANT { { SELECT | INSERT | UPDATE | REFERENCES } ( column_name [, ...] )
    [, ...] | ALL [ PRIVILEGES ] ( column_name [, ...] ) }
    ON [ TABLE ] table_name [, ...]
    TO role_specification [, ...] [ WITH GRANT OPTION ]
    [ GRANTED BY role_specification ]
```

---

On peut gérer les droits au niveaux des lignes:

```
CREATE POLICY name ON table_name
    [ AS { PERMISSIVE | RESTRICTIVE } ]
    [ FOR { ALL | SELECT | INSERT | UPDATE | DELETE } ]
    [ TO { role_name | PUBLIC | CURRENT_ROLE | CURRENT_USER | SESSION_USER } [, ...] ]
    [ USING ( using_expression ) ]
    [ WITH CHECK ( check_expression ) ]
```

- `using_expression` étant une expression renvoyant un booléen par ligne de `table_name`
- `check_expression` étant une expression renvoyant un booléen par ligne à insérer / update

## Les sauvegardes
Une sauvegarde doit être testée régulièrement.

### Sauvegardes logique
Des requètes SQL d'export des tables permettent de générer une
sauvegarde __à chaud__.

### Sauvegarde physique
Les fichiers de base peuvent être copiés __à froid__, à condition
d'arreter le SGBDR le temps de la copie.

### Sauvegarde PITR
Copie les données et les WAL en temps réel.

## L'auditabilité
Il est possible de sauvegarder les requêtes SQL d'un ou plusieurs
utilisateurs, pour des raisons légales ou d'audit sur des données
sensibles.

Ces données sont dans l'idéal déposées sur un serveurs distant géré
par d'autres administrateurs.

# Comparaison de SGBDR
![Utilisation des SGBDR (www.db-engines.com)](img/sgbd_compare.png){width=80%}

# Les outils connexes
## Les ORM
Il y a une alternative au SQL et aux procédures stockées, imbriquées dans le code source.

### Mapping Objet-Relationnel
Code source permettant d'abstraire le modèle relationnel en
programmation objet. Les requêtes SQL et les interactions avec le SGBDR
sont gérés par l'outil.

###

::: columns
:::: column

- `+` homogénéité du code source objet
- `+` le développeur peut ne pas connaître SQL
- `-` pas de maîtrise fine pour optimisation des interactions

::::

:::: column

Des examples d'ORM:

- **python**: sql-alchemy, django
- **java**: JPA, hibernate

::::
:::


---

Example sql-alchemy :
```python
class Department(Base):
    __tablename__ = 'department'
    id = Column(Integer, primary_key=True)
    name = Column(String)

class Employee(Base):
    __tablename__ = 'employee'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    department_id = Column(Integer, ForeignKey('department.id'))
    department = relationship(Department, backref=backref('employees'))

d = Department(name="IT")
emp1 = Employee(name="John", department=d)
s = session()
s.add(d)
s.add(emp1)
s.commit()
s.delete(d)
s.commit()
s.query(Employee).all()
```
## Gestionnaire de version de modèle
Le modèle relationnel est prévu pour évoluer. Un signe de succès d'un
projet est l'ajout de fonctionnalités. Des outils sont disponibles
pour gérer ces évolutions de schéma:

- liquibase
- flyway
- alembic

### Gestion des versions
Chaque DDL est historisé et versionné pour bénéficier des systèmes de
versionning de code: auditabilité, documentation, recherche d'erreur,
montées de versions.

---

Example d'alembic:
```bash
$ alembic history --verbose

Rev: ae1027a6acf (head)
Parent: 1975ea83b712
Path: /path/to/yourproject/alembic/versions/ae1027a6acf_add_a_column.py

    add a column

    Revision ID: ae1027a6acf
    Revises: 1975ea83b712
    Create Date: 2014-11-20 13:02:54.849677

Rev: 1975ea83b712
Parent: <base>
Path: /path/to/yourproject/alembic/versions/1975ea83b712_add_account_table.py

    create account table

    Revision ID: 1975ea83b712
    Revises:
    Create Date: 2014-11-20 13:02:46.257104
```

## Les éditeurs sql
Un éditeur sql est important:

- voir les objets (bases, tables, indexes...)
- éditer, formatter, sauvegarder les requetes SQL
- preview des résultats
- authentification, connections

Certains sont spécialisés au SGBDR, d'autres universels. Certains IDE
ont des plugins SQL (eg: intellij)

## DBeaver: compatible multi-SGBDR
![DBeaver: gère la plupart des SGBDR](img/dbeaver.png){width=85%}

# Les métiers des SGBDR
::: columns
:::: column

### Chef de projet
Recueille les besoins fonctionnels, organise les développements et
optimise le fonctionnement des équipes.

### Architecte
Conçoit l'application, écrit le code applicatif à l'interface avec la
base de données, les procédures stockées.


::::
:::: column
### Développeur
Implémente le modèle de données, les procédures stockées, document et
fait évoluer les différents codes sources.

### DBA
Administre la bdd, réalise la maintenance, les sauvegardes, le
monitoring, examine et remonte les requêtes problématiques.


::::
:::
# Conception de base de données
Les étapes de conception:

1. Perception du monde réel et capture des besoins
2. Élaboration du schéma conceptuel
3. Conception du schéma logique
4. Affinement du schéma logique
5. Élaboration du schéma physique

## Le modèle entité-relation
::: columns
:::: column

### Histoire
Inventé par Peter Chen en 1976 pour représenter les objets métiers et
modéliser les bases de données relationnelles.

- entité
- relations
- cardinalités
- propriétés

::::

:::: column
Il y a 3 niveaux :

- modèle conceptuel
- modèle logique
- modèle physique
::::
:::

## Le schema conceptuel

![Association entre voiture et personne](img/schema-concept.png){width=70%}

---

![Notation des cardinalités](img/card.png){width=40%}

## Le schema logique

Les règles:

1. Une entité est représentée par une relation (table) de même nom
   ayant pour attributs la liste des attributs de l’entité.
2. Une association est représentée par une relation de même nom ayant
   pour attributs la liste des clés des entités participantes et les
   attributs propres de l’association.

- PERSONNE (**NSS**, NOM, PRENOM, DATENAIS)
- VOITURE (**NV**, MARQUE, TYPE, PUISSANCE, COULEUR)
- POSSEDE (**NSS**, **NV**, DATE, PRIX)


## Le schema physique
### Definition
C'est la modélisation avec les objets du SGBDR: tables, indexes, clés,
colonnes... Plusieurs implémentations existent pour un même schema
logique.

---

![](img/schema-phy.png){width=70%}

## Dépendances fonctionnelles
La notion de dépendance fonctionnelle fut introduite dès le début du
relationnel par CODD afin de caractériser des relations pouvant être
décomposées sans perte d’informations.


### Definition
un attribut (ou groupe d’attributs) Y dépend fonctionnellement d’un
attribut (ou groupe d’attributs) X, si, étant donné une valeur de X,
il lui correspond une valeur unique de Y (quel que soit l’instant
considéré).

## Formes normales

> Les trois premières formes normales ont pour objectif de permettre la
> décomposition de relations sans perdre d’informations, à partir de la
> notion de dépendance fonctionnelle (Codd 1972). L’objectif de cette
> décomposition est d’aboutir à un schéma conceptuel représentant les
> entités et les associations canoniques du monde réel.

---

### Forme normale 1
> Une relation est en première forme normale si tout attribut contient
> une valeur atomique.

Elle permet simplement d'obtenir des tables rectangulaires sans
attributs multivalués irréguliers.

![](img/normal-1.png){width=40%}

---

### Forme normale 2
> Une relation R est en deuxième forme normale si et seulement si :
>
> 1. Elle est en première forme.
>
> 2. Tout attribut n’appartenant pas à une clé ne dépend pas d’une partie d’une clé.

Elle permet d’assurer l’élimination de certaines redondances en
garantissant qu’aucun attribut n’est déterminé seulement par une
partie de la clé.

---

![](img/normal-2.png){width=50%}

### Illustration
FOURNISSEUR (**NOM**, ADRESSE, **ARTICLE**, PRIX)

peut etre décomposé en:

FOURNISSEUR (**NOM**, ADRESSE)
PRODUIT (**NOM**, **ARTICLE**, PRIX)

---

### Forme normale 3
> Une relation R est en troisième forme normale si et seulement si :
>
> 1. Elle est en deuxième forme.
>
> 2. Tout attribut n’appartenant pas à une clé ne dépend pas d’un autre attribut non clé.

Elle permet d’assurer l’élimination des redondances dues aux
dépendances transitives.

---

![](img/normal-3.png){width=50%}

### Illustration
VOITURE (**NV**, MARQUE, TYPE, PUISSANCE, COULEUR)

peut etre décomposé en:

VOITURE (**NV**, TYPE, COULEUR)

MODELE (**TYPE**, MARQUE, PUISSANCE).


# Références

- Bases de Données - Georges Gardarin
- Designing Data-Intensive Applications - Martin Kleppmann
- PostgreSQL Documentation
- Wikipedia
