---
title: Les bases NO-SQL
author: Nicolas Paris
institute: Université Parix VIII
topic: Entrepôts de Données
theme: CambridgeUS
colortheme: crane
fonttheme: professionalfonts
outertheme: infolines
fontsize: 11pt
urlcolor: red
linkstyle: bold
aspectratio: 169
date-course: 09/11/2024
section-titles: false
toc-title: Plan du cours
toc: true
---

<!--
 see https://github.com/alexeygumirov/pandoc-beamer-how-to
 -->

# L'émergence du big-data
L'émergence des géants du numérique autour de 2010, a bousculé les
besoins en termes de bases de données :

- des millions d'utilisateurs
- sur tous les continents
- des contenus et des interactions complexes
- des besoins élastiques

---

Les challenges des **3V** :

### Volume
Fréquence d'acquisition de données pour un nombre d'utilisateurs
colossaux.

### Variété
Typologie des données (graphes sociaux, texte libre, images, sons,
vidéos, signaux) et des métadonnées (tracking clicks, visionnage,
types de devices et ip, mouvements souris)

### Vélocité
Interactions instantanées avec les systèmes, captation de l'attention
par l'afflux d'information.

# Les systèmes distribués

Les ressources d'un seul serveur n'étant pas illimitées, l'informatique distribuée est la seule approche pour répondre
aux challenges du big-data : faire collaborer des grappes de serveurs.

### Réplication

La réplication permet à chaque nœud d'avoir une copie identique de toutes les données. Elle apporte de la redondance et
des performances améliorées en consultation.

Il existe trois types de réplication sur la grappe :

- **mono-leader**: un seul leader accepte les écritures
- **multi-leader**: plusieurs leaders acceptent les écritures
- **leaderless**: toute la grappe accepte les écritures

---

La réplication entre les nœuds peut se faire de plusieurs manières :

- réplication via les logs : à partir des WAL qui sont envoyés par le réseau sur différents nœuds
- réplication par "change data capture" : des informations telles que la clé et les nouvelles valeurs (update), les
  clé/valeurs (insert), la clé (delete) sont envoyées
- réplication des instructions : les insertions / mises à jour des clients sont répliquées sur les nœuds
- réplication par triggers : un déclencheur envoie les données

---

### Partitionnement

Contrairement à la **réplication**, le **partitionnement** permet d'avoir une partie des données réparties sur plusieurs
nœuds. Utile lorsque le nombre d'entités dans la base est énorme et ne peut pas tenir sur un seul nœud en terme d'espace
et cout d'accès.

Il est très utilisé dans les systèmes OLAP vu au chapitre 3 du cours.

---

Les systèmes distribués ont des avantages :

- puissance de calcul élastique et bon marché
- tolérance aux pannes
- faibles latences (en rapprochant les data-center des clients)

Ils ont néanmoins des faiblesses :

- pannes réseaux / matérielles / logicielles
- de-synchronisations des horloges
- pauses (machines virtuelles, java garbage-collector, I/O asynchrones...)
- gestion des conflits de données

# Le théorème CAP
En 2000, Brewer énonce le théorème CAP au sujet des **systèmes distribués**.

- **C**onsistance: Toutes les écritures se font au même moment sur les
  nœuds actifs
- **A**vailable: Toutes les lectures/écritures fonctionnent avec
  succès sur les nœuds actifs
- **P**artition Tolerant: Lors d'une partition, toutes les
  communications sont coupées entre au moins deux groupes de la grappe

### Théorème CAP
Lors d'une **P**artition du réseau (coupure), un système distribué ne peut pas être à la fois **A**vailable et **C**onsistant.

---

![](img/cap-dc.png){width=80%}

L'un ou l'autre :

- **A**: Les deux parties sont disponibles, avec deux versions incohérentes
- **C**: Une partie n'est plus disponible, avec une seule version cohérente

# ACID distribué
Rappel : ACID dans le cas des SGBDR non distribuées. C'est une gestion
concurrentielle dite **Pessimiste** :

- **A**vailable: ne peut pas être découpé en plusieurs morceaux
- **C**onsistent: la base est dans un bon état
- **I**solation: les processus concurrents sont considérés comme séquentiels
- **D**urable: les données sont en lieu sûr

Il existe des implémentations ACID des transactions distribuées.

- 2 phases commit
- 3 phases commit
- Paxos

## 2 phases commit

Protocole décrit par Gray en 1978:

1. Un **coordinateur** demande au reste des **nœuds** leur accord pour faire un commit. Chaque nœud répond au
   coordinateur son accord/désaccord pour faire le commit.
2. En cas d'accord général, le **coordinateur** demande le commit à tous les **nœuds**. Si au moins un désaccord, alors
   le **coordinateur** demande aux nœuds un rollback général. Si un nœud a un problème (pour appliquer son commit ou
   rollback), le coordinateur lui re-demande jusqu'à ce qu'il y parvienne.

---

![](img/2-pc.png){width=100%}

---

Problème : que se passe-t-il si le coordinateur tombe en panne ?

Les nœuds attendent jusqu'à ce qu'il redémarre, et conservent un lock
sur les données pour la transaction en cours.

Le protocole paxos résoud cette limite, mais en règle générale, les
transactions distribuées sont lentes : facteur 10 pour une base Mysql
cluster par rapport à standalone.

# BASE

C'est une gestion concurrentielle dite **Optimiste**:

### **B**asically **A**vailable
L'écriture et la lecture sont possibles autant que possible, mais
parfois de manière non consistante.

### **S**oft-State
L'état n'est correct qu'avec une certaine probabilité.

### **E**ventual Consistency
Après un certain temps après l'écriture, toutes les lectures
retournent une seule et même valeur ; il y a convergence.

---

Il existe plusieurs qualités d'**eventual consistency** :

- **read your own write**: un utilisateur donné doit lire en tenant au moins compte de ses écritures
- **monotonically read**: une lecture se fait sur des versions croissantes

---

![](img/eventual-cons.png){ width=80% }

---

La réconciliation des écritures concurrente est nécessaire pour rétablir la consistance. Plusieurs approches existent :

### Read Repair

La correction est faite lorsque la lecture trouve une inconsistance. Cela ralenti les lectures.

### Write repair

La correction est faite lorsque l'écriture trouve une inconsistance. Cela ralenti les écritures.

### Asynchroneous Repair

La correction est gérée par un service indépendant des lectures/écritures.


# Le NO-SQL

![](img/freaks.png){ width=100% }


---

### NO-SQL
Terme apparu en 2009: **NO** puis **Not-Only** SQL, à prendre au sens
prise de distance avec le **modèle relationnel**.

- capacité à "scaler" (distribution)
- éviter les contraintes de la modélisation relationnelle
- réaliser des opérations non prévues par le SQL
- engouement pour le FOSS (Free/Open Source Software)

# Les bases document

::: columns
:::: column

![](img/document.png){ width=100% }

::::

:::: column

Exemple:

- mongodb
- couchdb

::::
:::


---

![](img/document-mongo.png){ width=90% }

---

Un **document** contient le profil complet (jobs, cars) associé à l'identifiant unique : le `user_id`.

Dans le modèle relationnel on obtient plusieurs tables (user, jobs, cars ...)

---

::: columns
:::: column

![](img/car.png){ width=90% }

::::

:::: column

Cas des relations **N:M** ?

1. répétition des informations dans les deux collections
2. utilisation de tableaux de références dans chacones

::::
:::

---

::: columns
:::: column
N:M par **redondance**

### Collection Owner
```json
{"owner": "john", "age": 65, "cars": [{"type": "mercedes", "year":  1999}, {"type": "citroen", "year": 2003}]}
```

### Collection Cars
```json
{"type": "mercedes", "year": 1999, "owners": [{"owner": "john"}, {"owner": "jim"}]}
{"type": "citroen", "year": 2003, "owners": [{"owner": "john"}, {"owner": "jim"}]}
```

::::

:::: column

N:M par **références**

### Collection Owner
```json
{"id":10, "owner": "john", "age": 65, "cars": [1, 3]}
{"id":12, "owner": "jim", "age": 65, "cars": [1, 3]}
```

### Collection Cars
```json
{"id":1, "type": "mercedes", "year": 1999, "owners": [10, 12]}
{"id":3, "type": "citroen", "year": 2003, "owners": [12, 13]}
```

::::
:::

---

- `+` les données d'un même document sont sur le disque même
  emplacement disque
- `+` format json maîtrisé par les dev web frontend et backend si
  node-js
- `+` partitionnement (grappe serveurs) des collections simplifié par l'indépendance
- `-` adapté aux relations 1:N (arbre)
  entre les documents/collections
- `-` pas de jointures
- `-` pas adapté aux relations N:1, N:M
- `-` dénormalisation (redondance) des données dans plusieurs
  collections
- `-` pas de transactions multi-documents
- `-` pas de contraintes d'intégrité
- `-` nombre limité d'index par collection


# Les bases clé / valeur
### Clé / Valeur

Bases s'appuyant sur une structure de données optimisée pour l'accès via une **clé**.

Certaines technologies proposent des modes d'indexation secondaires dont l'efficacité est limitée.

La **valeur** est parfois structurée pouvant rappeler des colonnes : on parle de bases clé/valeur orientées colonnes.

## Les bases SSTables
### Sorted String Table

Les données sont stockées en mémoire. Lorsqu'un seuil est dépassé, elles sont persistées dans un fichier ordonnés selon
une clé : un segment sstTable. Un index renseigne la localisation des clés dans les segments.

Une lecture dans la base cherche la clé en mémoire et sur disque.

![](img/sstable1.png){width=100%}

---

![Régulièrement les nouveaux fichiers sont compactés](img/sstable2.png){width=70%}

---

Avantages / inconvénients des ssTables:

- `+`: gestion immutable des segments (append only)
- `+`: lectures séquentielles efficaces sur disque
- `-`: interrogations par clé uniquement

---

Il existe de nombreuses bases clé/valeurs basées sur des ssTables:

FOSS:

- cassandra / scylladb (eventual consistent)
- hbase (Strong consistent)

Propriétaires :

- dynamodb (amazon)
- big table (google)
- cosmosdb (microsoft)

## Hbase
![Architecture Hbase](img/hbase.png){width=70%}

---

Hbase fournit une API pour le CRUD (Create, Read, Update, Delete) :

### Écrire
Syntax:  put <'tablename'>,<'rowname'>,<'columnvalue'>,<'value'>

On ne peut écrire qu'une seule colonne par appel à l'API

### Lire (Lookup)
Un get permet de récupérer les informations d'une ligne via la clé.

Syntax: get <'tablename'>, <'rowname'>, {< Additional parameters>}

Additional-parameters: TIMERANGE, TIMESTAMP, VERSIONS and FILTERS


### Lire (Scan)
Un scan permet de récupérer de nombreuses lignes. C'est lent.

Syntax: scan <'tablename'>, {Optional parameters}

Optional-parameters: TIMERANGE, FILTER, TIMESTAMP, LIMIT, MAXLENGTH, COLUMNS, CACHE, STARTROW and STOPROW

## Cassandra
![Architecture Cassandra](img/cassandra.png){width=70%}

---

### Le langage CQL de cassandra
- s'approche du SQL
- il ne permet pas de jointures
- les **group-by** / **order-by** ne peuvent que se faire sur la clé primaire
- indexes secondaires limités (à utiliser en conjonction avec la clé primaire)

---

```sql
select_statement::= SELECT [ JSON | DISTINCT ] ( select_clause | '*' )
	FROM `table_name`
	[ WHERE `where_clause` ]
	[ GROUP BY `group_by_clause` ]
	[ ORDER BY `ordering_clause` ]
	[ PER PARTITION LIMIT (`integer` | `bind_marker`) ]
	[ LIMIT (`integer` | `bind_marker`) ]
	[ ALLOW FILTERING ]
select_clause::= `selector` [ AS `identifier` ] ( ',' `selector` [ AS `identifier` ] )
selector::== `column_name`
	| `term`
	| CAST '(' `selector` AS `cql_type` ')'
	| `function_name` '(' [ `selector` ( ',' `selector` )_ ] ')'
	| COUNT '(' '_' ')'
where_clause::= `relation` ( AND `relation` )*
relation::= column_name operator term
	'(' column_name ( ',' column_name )* ')' operator tuple_literal
	TOKEN '(' column_name# ( ',' column_name )* ')' operator term
operator::= '=' | '<' | '>' | '<=' | '>=' | '!=' | IN | CONTAINS | CONTAINS KEY
group_by_clause::= column_name ( ',' column_name )*
ordering_clause::= column_name [ ASC | DESC ] ( ',' column_name [ ASC | DESC ] )*
```
---

![Modelisation "Chebotko" pour cassandra](img/cassandra-logical.png){width=80%}

---

La modélisation **Chebotko** permet de décrire la modélisation logique du parcours utilisateur suivant :

1. on recherche les hôtels par position (POI) dans **hotel_by_poi**
2. on cherche les infos d'un hôtel donné par _hotel-id_ dans **hotel**
3. on cherche les positions proches d'un _hotel-id_ dans **pois_by_hotel**
4. on cherche les chambres libres pour _hotel-id_ sur une plage de date dans **available_rooms_by_hotel_date**
5. on cherche les services offerts par une _room-id_ dans un _hotel-id_ dans **amenities_by_room**

## Les bases clé / valeurs

Il existe d'autres implémentations de bases clé / valeurs qui ne sont pas basées sur des ssTables:

- fondationDB : Base distribuée offrant des transactions ACID via l'algorithme Paxos.
- redis : Base "in memory" offrant des performances de lecture souvent exploitées pour la mise en cache

# Les bases graph

## Les Triples Store

::: columns
:::: column

### Le Web Semantique

Aussi nommé web 3, il consiste à structurer l'information des sites web html, pour un web des données. Wikidata est un
exemple opérationnel implementation.

### Les triplets

On parle de triplets, **sujet**, **prédicat**, **objet**. Le sujet est souvent sous forme d'une URL de part ses origines
web. Par exemple, `Nicolas | aime  | les Napolitaines` est un triplet valide.

::::
:::: column

### Ontologie

Une ontologie est une modélisation du réel via des classes, des cardinalités et des règles (assimilables à des
contraintes d'intégrités). Par exemple, l'Ontologie des Pizza, avec chaque type et ingrédients.

### Instances

Les instances sont des triplets correspondants aux ontologies. Un **triple store** est une base permettant de stocker et
de requêter les instances, sous forme de triplets. Par exemple, un triple store pour gérer le stock et les commandes de pizza.

::::
:::

---

![Des triplets RDF](img/rdf.png){width=70%}

---
Les mêmes triplets sérialisés au format turtle :

```
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix contact: <http://www.w3.org/2000/10/swap/pim/contact#>.

<http://www.w3.org/People/EM/contact#me>
  rdf:type contact:Person;
  contact:fullName "Eric Miller";
  contact:mailbox <mailto:em@w3.org>;
  contact:personalTitle "Dr.".
```

---

::: columns
:::: column

Une requête **SELECT** renvoie une colonne pour chaque variable, et une ligne pour chaque pattern qui matche le graph.
```sparql
SELECT <variables>
WHERE {
   <graph pattern>
}
```


Une requête upsert (insert/update si-existe):
```sparql
INSERT DATA
{
  <book1> dc:title "A_new_book" ;
          dc:creator "AN_Other" .
}
```

::::
:::: column

Un exemple de graph-pattern :
```sparql
SELECT *
{
   ?album a :Album .
   ?album :artist ?artist .
   ?artist a :SoloArtist .
}
```

|   album   |     artist     |
|:---------:|:--------------:|
| McCartney | Paul_McCartney |
|  Imagine  |   John_Lennon  |

::::
:::


---

::: columns
:::: column

Aggregation:
```sparql
SELECT ?year (count(distinct ?album) AS ?count)
{
    ?album a :Album ;
            :date ?date ;
    BIND (year(?date) AS ?year)
}
GROUP BY ?year
ORDER BY desc(?count)
```
::::
:::: column

| year | count |
|:----:|:-----:|
| 2021 |   98  |
| 2020 |   76  |
| 2019 |   80  |

::::
:::
## Les Property Graph

::: columns
:::: column

![](img/bdd-graph.jpg){width=100%}

::::
:::: column

Neo4J est une base de graphe à propriétés :

- une **vertice** : Un élément du graphe, associé à une collection de propriétés
- une **edge** : Un lien entre deux éléments, associé à un label et une collection de propriétés

::::
:::

---

Avantages et inconvénients :

- `+`: Tout comme pour les triples stores, le schema du graph est évolutif.
- `+`: des requêtes complexes peuvent être rédigées simplement
- `+`: transactions ACID
- `-`: non distribué : distribuer un graphe n'est pas optimal
- `-`: modèle spécifique, inutile pour des données tabulaires sans liens profonds

---

Le langage CYPHER

# Les moteurs de recherche

## Lucene
### Lucene

Bibliothèque de programmation écrite en java permettant d'indexer des documents y compris full-texte.

Lucene stocke les documents dans des segments de ssTable qui associe à la clé ordonnée `document-id`, le contenu du
document.

De plus, il utilise un index inversé, qui pour chaque valeur associe les `document-id`.

---

![Indexation Lucene](img/sstable-lucene.png){width=80%}

---

![Indexation Lucene](img/lucene.png){width=80%}

## Elasticsearch et Solr

Lucene :

- `+` permet des aggregations précalculées (facetting)
- `+` peut indexer des documents avec des champs complexes (dates, entiers, coordonnés GPS)
- `-` est near-real time (le memory store n'est pas requêtable)
- `-` transactionnel au niveau d'un document seulement (ajout, modification, suppression)

**Solr** et **ElasticSearch** sont des bases de données spécialisées basées sur **Lucène** avec comme avantages :

- distribution des index lucene sur plusieurs nœuds
- réplication et partitionnement des données
- API de consultation
- gestion des droits d'accès
- gestion des sauvegardes

# Les bases de données temporelles

### Time-series-Databases

Certaines données (objets connectés, capteurs, electro-encéphalogrammes/cardiogrammes) produisent des données dont
l'unité est le `Hertz`

Des bases spécialisées existent pour stocker et analyser en temps réel ces informations.

## TimestaleDB
- Postgresql extension
- fonctions specifiques pour les séries temporelles
- scalabilité multi-nodes

### Optimisations performances
- tables d'aggregations temps reel
- tierce storage (deplacer les anciennes données sur s3 au format parquet)
- partitionnement des tables par plages de dates

## Warp10
Warp10 est FOSS et développée en Bretagne. C'est en réalité une surcouche de
bases clé-valeur: leveldb (standalone) ou foundationDB (distribué).


## Prometheus
Base de série temporelle développée par SoundCloud initialement.


# Les bases géo-spaciales

## PostGis
- Postgresql extension
- fonctions spécifiques aux manipulations geo-spaciales

## Omnisci


# Références

- Designing Data-Intensive Applications - Martin Kleppmann
- Cassandra Documentation
- Neo4J Documentation
- Wikipedia
